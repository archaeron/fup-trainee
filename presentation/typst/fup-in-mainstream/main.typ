#import "@preview/polylux:0.3.1": *
#import themes.university: *

#show: university-theme

#set quote(block: true)

#title-slide(
  title: "Teil 3: Funktionale \"Patterns\" in Mainstream Sprachen",
  authors: ("Nicolas Gagliani", "Dandolo Flumini"),
  institution-name: ""
)

#slide(
    title: "Void Safety - Maybe Typ",
)[
  = Wir wollen NullPointerExceptions vermeiden

  Einige Sprachen haben oder sind am "Void Safety" einbauen

  - C\# - #link("https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/builtin-types/nullable-value-types")[Nullable Value Types]
  - Dart - #link("https://dart.dev/null-safety")[Null Safety]
  - TypeScript - #link("https://www.typescriptlang.org/docs/handbook/2/everyday-types.html#null-and-undefined")[Nullable Types]
  - Kotlin
  - Swift
]

#slide(
    title: "Void Safety - Durch sum types",
)[
  = Elm
  ```elm
  nullableString : Maybe String
  nullableString = Nothing
  ```
  = TypeScript
  ```ts
  const nullableString: string | null = null;
  ```
  = Swift
  ```swift
  let nullableString: String? = nil;
  // sugar for
  // let nullableString: Optional<String> = nil;
  ```
]

#slide(
    title: "Void Safety - Ad Hoc",
)[
  = C\#
  ```cs
  string? nullableString = null;
  ```
  = Dart
  ```dart
  String? nullableString = null;
  ```
 = Kotlin
  ```kotlin
  var nullableString: String? = null
  ```
]

#slide(
    title: "Pure Funktionen",
)[
  Funktionen ohne Nebeneffekte sind einfacher zu testen,
  aber auch zu verändern.

  ```py
  def addInfo(info: str) -> None:
      person.info = info
  ```

  ```py
  def addInfo(
      person: Person,
      info: str
  ) -> Person :
      return Person(
          name = person.name,
          info = info
      )
  ```
]

#slide(
    title: "Anonyme Funktionen und Map",
)[
  = Elm
  ```elm
  xs = ["one", "two", "three"]
  ux = List.map String.toUpper xs
  ```
]

#slide(
    title: "Anonyme Funktionen und Map",
)[
  = C\#
  ```cs
  List<string> xs =
      new() { "one", "two", "three" };
  List<string> us =
      xs
          .Select((string s) => s.ToUpper())
          .ToList();
  ```
]

#slide(
    title: "Anonyme Funktionen und Map",
)[
  = C\# - Linq
  ```cs
  List<string> xs =
      new() { "one", "two", "three" };
  List<string> us =
      (from x in xs
      select x.ToUpper()).ToList();
  ```
]

#slide(
    title: "Anonyme Funktionen und Map",
)[
  = JavaScript
  ```js
  const xs = ["one", "two", "three"]
  const us = xs.map(s => s.toUpperCase())
  ```
  = TypeScript
  ```ts
  const xs = ["one", "two", "three"]
  const us =
      xs.map((s: string) => s.toUpperCase())
  ```
]

#slide(
    title: "Anonyme Funktionen und Map",
)[
  = Java
  ```java
  List<String> xs =
      Arrays.asList("one", "two", "three");
  List<String> us =
      xs
          .stream()
          .map((s) -> s.toUpperCase())
          .collect(Collectors.toList());
  ```
]

#slide(
    title: "Anonyme Funktionen und Map",
)[
  = Python
  ```java
  xs = ["one", "two", "three"]
  us = list(map(lambda s: s.upper(), xs))
  ```
]

#slide(
    title: "Algebraische Datentypen (ADT)",
)[
  ```ts
  type Error = { message: string }

  type Person = {
      name: string
      hasError: boolean
      error: Error | null
  }
  ```
  vs
  ```ts
  type Person = Error | {
      name: string
  }
  ```
]

#slide(
    title: "Datentypen",
)[
  - Java hat `Optional<T>`, kann aber auch `null` sein!
  - #link("https://www.vavr.io/")[vavr (Java)]
  - #link("https://immutables.github.io/")[Immutables (Java)]
  - #link("https://immutable-js.github.io/immutable-js/")[immutable (JavaScript)]
  - #link("https://github.com/immerjs/immer")[immer (JavaScript)]
]

#slide(
    title: "Datentypen",
)[
= C\#
  ```cs
  public record struct Person(
      string FirstName,
      string LastName
  );
  var person = new Person("Bill", "Wagner");
  Person brother =
      person with
          { FirstName = "Paul" };
  ```
]

#slide(
    title: "Datentypen",
)[
  = Java
  ```java
  record Point(int x, int y) { }
  ```
]
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#slide(
    title: "Parse, don't validate",
)[
  = Grundidee

  Basierend auf #link("https://lexi-lambda.github.io/blog/2019/11/05/parse-don-t-validate/")[Alexis Kings Artikel].

  Beim validieren von Daten, diese gerade in
  eine nützliche Form bringen, also parsen.

  Dies geschieht am besten an den "Rändern" unseres Programms.

  - User input
  - Datenbank
  - Http Request
]

#slide(
    title: "Parse, don't validate",
)[
  = Beispiel - Vorher

  ```elm
  validateNonEmpty : [a] -> Maybe String
  validateNonEmpty xs = case xs of
      (_::_) -> Nothing
      []     -> Just "list cannot be empty"
  ```
]

#slide(
    title: "Parse, don't validate",
)[
  = Beispiel - Nacher

  ```elm
  type NonEmpty a = NonEmpty a (List a)

  parseNonEmpty : [a]
      -> Result String (NonEmpty a)
  parseNonEmpty xs = case xs of
      (y::ys) -> Ok (NonEmpty y ys)
      []      -> Err "list cannot be empty"
  ```
]

#slide(
    title: "Parse, don't validate",
)[
   = "Shotgun Parsing" - 1

  #quote(
    attribution: [
        #link("http://langsec.org/papers/langsec-cwes-secdev2016.pdf")[The Seven Turrets of Babel: A Taxonomy of LangSec Errors and How to Expunge Them]
      ]
  )[
    Shotgun Parsing: Shotgun parsing is a programming antipattern whereby
    parsing and input-validating code is mixed with and spread across processing
    code --- throwing a cloud of checks at the input, and hoping, without
    any systematic justification, that one or another would catch all the "bad" cases.
  ]
]

#slide(
    title: "Parse, don't validate",
)[
   = "Shotgun Parsing" - 2

  #quote(
    attribution: [
        #link("http://langsec.org/papers/langsec-cwes-secdev2016.pdf")[The Seven Turrets of Babel: A Taxonomy of LangSec Errors and How to Expunge Them]
      ]
  )[
  Shotgun parsing necessarily deprives the program of the ability to reject
  invalid input instead of processing it. Late-discovered errors in an input
  stream will result in some portion of invalid input having been processed,
  with the consequence that program state is difficult to accurately predict.
  ]
]

#slide(
    title: "TypeScript",
)[
  TypeScript is speziell, da die Sprache zwar union types hat, diese jedoch
  untagged sind.

  ```ts
  type IntOrString = int | string
  ```
  TypeScript kann sogar Schnittmengen darstellen.

  ```ts
  type Named<a> = a & { name: string}
  ```
]

#slide(
    title: "TypeScript",
)[
  "Literal types" sind sehr nützlich um JavaScript zu typisieren.

  ```ts
  type YesNo = "Yes" | "No"

  function setVerticalAlignment(
      location: "top" | "middle" | "bottom"
  ) {
      // ...
  }
  ```
]


#slide(
    title: "TypeScript",
)[
  Wenn wir eine Tagged Union wollen, müssen wir die selber erstellen.

  Elm:
  ```elm
  type Result err ok = Err err | Ok ok
  ```
  TypeScript:
  ```ts
  type Result<err, ok>
      = { type: "error", error: err }
      | { type: "ok", ok: ok }

  type ResultTypes = Result<never, never>["type"]
  // type ResultTypes = "error" | "ok"
  ```
]


#slide(
    title: "TypeScript",
)[
  Sogar Typen zu Compilezeit erstellen ist möglich
  ```ts
  type VerticalAlignment = "top" | "bottom";
  type HorizontalAlignment = "left" | "right";
  type Aligment = `${VerticalAlignment}-${HorizontalAlignment}`
  // type Aligment =
  //   | "top-left"    | "top-right"
  //   | "bottom-left" | "bottom-right"
  ```
]


#slide(
    title: "TypeScript",
)[
  Sogar Typen zu Compilezeit erstellen ist möglich
  ```ts
  type Todo = {
    title: string;
    description: string;
    completed: boolean;
  }

  type TodoPreview =
      Pick<Todo, "title" | "completed">;
  // type TodoPreview =
  //     { title: string;
  //       completed: boolean; }
  ```
]