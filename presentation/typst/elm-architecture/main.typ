#import "@preview/polylux:0.3.1": *
#import themes.university: *

#show: university-theme

#let exampleSlideColor = rgb("#0C6291")
#let exerciseSlideColor = rgb("#A63446")

#title-slide(
  title: "Elm Architecture",
  subtitle: "Functional Programming in Elm",
  authors: ("Nicolas Gagliani", "Dandolo Flumini"),
  institution-name: ""
)

//-----------------------------------------
// Schritt 0
//-----------------------------------------

#focus-slide(background-color: exampleSlideColor)[
  Schritt 0 - Beispiel
]

#slide(
    title: "Schritt 0",
    new-section: "Beispiel"
)[
    Im ersten Schritt erstellen wir eine statische Webseite mit
    zwei Knöpfen und einer Zahl, die jedoch noch nicht interaktiv sind.

    #align(center)[
        #image("images/v0-example.png")
    ]
]

#slide(
    title: "Schritt 0",
    new-section: "Beispiel"
)[
    #image("diagrams/v0.svg")
]


#slide(
    title: "Schritt 0",
    new-section: "Beispiel"
)[
    ```elm
    view : Html msg
    view =
        div []
            [ button [] [ text "+" ]
            , div [] [ text (String.fromInt 0) ]
            , button [] [ text "-" ]
            ]

    main : Html msg
    main =
        view
    ```
]

#focus-slide(background-color: exerciseSlideColor)[
  Schritt 0 - Übung
]

#slide(
    title: "Schritt 0",
    new-section: "Übung"
)[
    Öffne „Main.elm“ im Ordner „Exercise/src“ und erstelle mit Hilfe der folgenden Funktionen eine einfache
    Webseite mit einer Kopfzeile und einem Eingabefeld.

    Hinweise:
    - https://package.elm-lang.org/packages/elm/html/latest/Html#div
    - https://package.elm-lang.org/packages/elm/html/latest/Html#h1
    - https://package.elm-lang.org/packages/elm/html/latest/Html#input
]

//-----------------------------------------
// Schritt 1
//-----------------------------------------

#focus-slide(background-color: exampleSlideColor)[
  Schritt 1 - Beispiel
]

#slide(
    title: "Schritt 1",
    new-section: "Beispiel"
)[
    Nun machen wir die Knöpfe klickbar und zeigen die eingestellte Zahl an.

    #align(center)[
        #image("images/v1-example.png")
    ]
]

#slide(
    title: "Schritt 1",
    new-section: "Beispiel"
)[
    #image("diagrams/v1.svg")
]

#slide(
    title: "Schritt 1",
    new-section: "Beispiel"
)[
    ```elm
    type alias Model = { number : Int }

    type Msg = Increment | Decrement

    view : Model -> Html Msg
    view model =
        div []
            [ button [ onClick Increment ] [ text "+" ]
            , div [] [ text (String.fromInt (.number model)) ]
            , button [ onClick Decrement ] [ text "-" ]
            ]
    ```
]

#slide(
    title: "Schritt 1",
    new-section: "Beispiel"
)[
    ```elm
    type alias Model = { number : Int }

    type Msg = Increment | Decrement

    update : Msg -> Model -> Model
    update msg model =
        case msg of
            Increment ->
                { model | number = .number model + 1 }

            Decrement ->
                { model | number = .number model - 1 }
    ```
]

#slide(
    title: "Schritt 1",
    new-section: "Beispiel"
)[
    ```elm
    main : Program () Model Msg
    main =
        Browser.sandbox
            { init = initialModel
            , update = update
            , view = view
            }
    ```
]

#focus-slide(background-color: exerciseSlideColor)[
  Schritt 1 - Übung
]

#slide(
    title: "Schritt 1",
    new-section: "Übung"
)[
    Parse nun den Inhalt des Eingabefeldes in ein `Int`.
    Zeige ein Feld mit einer Fehlermeldung an, wenn der `Int` nicht geparst werden konnte.

    Wichtig: Auch wenn die Eingabe kein `Int` ist, wollen wir sie nicht verlieren.
    Stell sicher, dass der Wert im Eingabefeld bleibt, auch wenn er nicht geparst werden kann.

    Hinweise:
    - https://package.elm-lang.org/packages/elm/html/latest/Html-Events#onInput
    - https://package.elm-lang.org/packages/elm/core/latest/String#toInt
    - https://package.elm-lang.org/packages/elm/core/latest/Result#Result
]

//-----------------------------------------
// Schritt 2
//-----------------------------------------

#focus-slide(background-color: exampleSlideColor)[
  Schritt 2 - Beispiel
]

#slide(
    title: "Schritt 2",
    new-section: "Beispiel"
)[
    Im letsten Schritt haben wir die Seite interaktiv gemacht.
    Nun lernen wir wie man ein wiederverwendbares Stück der Seite
    in eine Komponente packen können.

    Übrigens: Komponenten sind nicht unbedingt nötig um eine
    Seite zu erstellen. Können aber nützlich sein um
    zum Beispiel standard Komponenten zur Verfügung zu stellen.

    #align(center)[
        #image("images/v0-example.png")
    ]
]

#slide(
    title: "Schritt 2",
    new-section: "Beispiel"
)[
    #image("diagrams/v2.svg")
]

#slide(
    title: "Schritt 2",
    new-section: "Beispiel"
)[
    Siehe `ElmArchitecture/V2`.
]

#focus-slide(background-color: exerciseSlideColor)[
  Schritt 2 - Übung
]

#slide(
    title: "Schritt 2",
    new-section: "Übung"
)[
    Aufbauend auf der Übung aus 'Schritt 1', konvertiere das Eingabefeld
    in eine Komponente.
    Verwende diese Komponente zusammen mit einer „Hinzufügen“-Schaltfläche, so dass du
    mehrere Eingabefelder hinzufügen kannst.
    Du kannst eine `List` verwenden, um den richtigen Wert des Eingabefeldes
    im Status zu aktualisieren.

    Hinweise:
    - https://package.elm-lang.org/packages/elm/core/latest/List
]

//-----------------------------------------
// Schritt 3
//-----------------------------------------

#focus-slide(background-color: exampleSlideColor)[
  Schritt 3 - Beispiel
]

#slide(
    title: "Schritt 3",
    new-section: "Beispiel"
)[
    Nun machen wir einen grösseren Sprung und senden eine Anfrage
    an einen Server. Wir senden ein Wort an den Server und dieser wird
    das Wort umkehren und uns zurückschicken.

    #align(center)[
        #image("images/v3-example.png")
    ]
]

#slide(
    title: "Schritt 3",
    new-section: "Beispiel"
)[
    Was wir neu brauchen:
    - #link("https://package.elm-lang.org/packages/elm/json/1.1.3/")[JSON]
    - #link("https://package.elm-lang.org/packages/elm/browser/latest/Browser#element")[Browser.element]
    - #link("https://package.elm-lang.org/packages/elm/http/2.0.0/")[Http]
]

#slide(
    title: "Schritt 3",
    new-section: "JSON"
)[
    ```elm
    import Json.Decode as D
    import Json.Encode as E

    type alias Cause =
        { name : String
        , percent : Float
        }
    ```
]

#slide(
    title: "Schritt 3",
    new-section: "JSON - Encoding"
)[
    ```elm
    import Json.Encode as E

    E.string : String -> Value
    E.float : Float -> Value
    E.object : List ( String, Value ) -> Value
    ```
]

#slide(
    title: "Schritt 3",
    new-section: "JSON - Encoding"
)[
    ```elm
    type alias Cause =
        { name : String
        , percent : Float
        }

    encodeCause : Cause -> E.Value
    encodeCause cause =
        E.object
            [ ("name", E.string cause.name)
            , ("percent", E.float cause.percent)
            ]

    E.encode : Int -> E.Value -> String
    ```
]

#slide(
    title: "Schritt 3",
    new-section: "JSON - Decoding"
)[
    ```elm
    import Json.Decode as D

    D.string : Decoder String
    D.float : Decoder Float
    D.field : String -> Decoder a -> Decoder a
    D.map2 :
        (a -> b -> value) ->
        Decoder a ->
        Decoder b ->
        Decoder value
    ```
]

#slide(
    title: "Schritt 3",
    new-section: "JSON - Decoding"
)[
    ```elm
    type alias Cause =
        { name : String
        , percent : Float
        }

    decoder : D.Decoder Cause
    decoder =
        D.map2 Cause
            (D.field "name" D.string)
            (D.field "percent" D.float)
    
    D.decodeString : D.Decoder a -> String -> Result D.Error a
    ```
]


#slide(
    title: "Schritt 3",
    new-section: "JSON"
)[
    ```elm
    encode : Int -> Value -> String
    decodeString : Decoder a -> String -> Result Error a
    ```

    - https://package.elm-lang.org/packages/elm/json/latest/Json-Encode#encode
    - https://package.elm-lang.org/packages/elm/json/latest/Json-Decode#decodeString
]


#slide(
    title: "Schritt 3",
    new-section: "Browser.element"
)[
    #link("https://package.elm-lang.org/packages/elm/browser/latest/Browser#element")

    Vorher:

    ```elm
    sandbox :
        { init : model
        , view : model -> Html msg
        , update : msg -> model -> model
        }
        -> Program () model msg
    ```
]

#slide(
    title: "Schritt 3",
    new-section: "Browser.element"
)[
    Nacher:

    ```elm
    element :
        { init : flags -> ( model, Cmd msg )
        , view : model -> Html msg
        , update : msg -> model -> ( model, Cmd msg )
        , subscriptions : model -> Sub msg
        }
        -> Program flags model msg
    ```
]

#slide(
    title: "Schritt 3",
    new-section: "Http"
)[
    ```elm
    Http.get :
        { url : String
        , expect : Http.Expect msg
        }
        -> Cmd msg

    Http.expectJson :
        (Result Http.Error a -> msg) ->
        Decoder a ->
        Http.Expect msg
    ```
]

#slide(
    title: "Schritt 3",
    new-section: "Beispiel"
)[
    Siehe `ElmArchitecture/V3`.
]

#focus-slide(background-color: exerciseSlideColor)[
  Schritt 3 - Übung
]

#slide(
    title: "Schritt 3",
    new-section: "Übung"
)[
    Nun werdet ihr ein mini App erstellen zum Ausgaben erfassen und damit alles Gelernte anwenden.

    #align(center)[
        #image("images/v3-exercise.png")
    ]

    Dazu habt ihr einen kleinen Beispielserver im Ordner
    `servers/expenses-server`.
]

#slide(
    title: "Schritt 3",
    new-section: "Übung"
)[
    API des test servers:

    - POST `/expenses` - Sende eine expense an den server (Format beliebig).
    - GET `/expenses` - Lädt alle expenses (Liste von expenses).

    Siehe: `servers/expenses-server/server.js`
]

#slide(
    title: "Schritt 3",
    new-section: "Übung"
)[
    Weitere Ideen:
    - Summe der Ausgaben
    - Styling (z.B. Elm UI)
    - Kategorisieren von Expenses
    - Routing

    Hinweise:
    - https://package.elm-lang.org/packages/mdgriffith/elm-ui/latest/
    - https://package.elm-lang.org/packages/elm/browser/latest/Browser#application
]