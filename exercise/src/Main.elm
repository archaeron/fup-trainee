module Main exposing (main)

import Browser
import Html exposing (Html, button, div, text)
import Html.Events exposing (onClick)



--------------------------------------------------------------------------------
-- Model
--------------------------------------------------------------------------------


{-| The `Model` contains all the data necessary
to display our application.
-}
type alias Model = ()


{-| We need an empty `Model` to start with.
-}
initialModel : Model
initialModel = ()


--------------------------------------------------------------------------------
-- Update
--------------------------------------------------------------------------------


{-| Whenever we interact with the page a message is sent to the
system.
-}
type Msg = NoOp


{-| We then update the `Model` according to an incoming Message.
-}
update : Msg -> Model -> Model
update msg model =
    case msg of
        NoOp ->
            ()



--------------------------------------------------------------------------------
-- View
--------------------------------------------------------------------------------


{-| The view renders the current state of the application from
a `Model`.
-}
view : Model -> Html Msg
view model =
    div []
        [ 
        ]



--------------------------------------------------------------------------------
-- Main
--------------------------------------------------------------------------------


{-| We put all pieces together in the `main` function.
-}
main : Program () Model Msg
main =
    Browser.sandbox
        { init = initialModel
        , update = update
        , view = view
        }
