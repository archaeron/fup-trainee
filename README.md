# Introduction to Elm

There is a very good introduction at https://elmprogramming.com
Great UI Library at: https://elm-ui.netlify.app/
Single Page App Example: https://github.com/rtfeldman/elm-spa-example

## Introduction

This folder contains an introduction to functional programming
using the Elm language.

# Elm Architecture

Slowly getting to know the elm architecture which is not only used in elm.

E.g.:

- https://ratatui.rs/concepts/application-patterns/the-elm-architecture/
- https://purplekingdomgames.com/blog/2024/03/05/deriving-the-elm-architecture

## Expenses Tracker

A small webpage in Elm to track expenses of a group.

We use an express server written in JavaScript
to save expenses.

### Start

Open two terminals:

- `cd expenses-server; npm run start`
- `bin/start`
