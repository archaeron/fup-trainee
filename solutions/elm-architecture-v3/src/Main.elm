module Main exposing (main)

import Browser
import Html exposing (Html, button, div, h1, li, text, ul)
import Html.Attributes exposing (list)
import Html.Events exposing (onClick)
import NumberInput



--------------------------------------------------------------------------------
-- Model
--------------------------------------------------------------------------------


{-| The `Model` contains all the data necessary
to display our application.
-}
type alias Model =
    { numbers : List NumberInput.Model
    }


{-| We need an empty `Model` to start with.
-}
initialModel : Model
initialModel =
    { numbers = [ NumberInput.initialModel ]
    }



--------------------------------------------------------------------------------
-- Update
--------------------------------------------------------------------------------


{-| Whenever we interact with the page a message is sent to the
system.
-}
type Msg
    = UpdateNumber Int NumberInput.Msg
    | AddInput


updateElementAt : Int -> (a -> a) -> List a -> List a
updateElementAt pos f list =
    List.indexedMap
        (\i m ->
            if i == pos then
                f m

            else
                m
        )
        list


{-| We then update the `Model` according to an incoming Message.
-}
update : Msg -> Model -> Model
update msg model =
    case msg of
        UpdateNumber position numberInputMsg ->
            { model
                | numbers =
                    let
                        upd =
                            NumberInput.update numberInputMsg

                        updatedList =
                            updateElementAt position upd model.numbers
                    in
                    updatedList
            }

        AddInput ->
            { model | numbers = NumberInput.initialModel :: model.numbers }



--------------------------------------------------------------------------------
-- View
--------------------------------------------------------------------------------


{-| The view renders the current state of the application from
a `Model`.
-}
view : Model -> Html Msg
view model =
    let
        numberInputs : List (Html Msg)
        numberInputs =
            model.numbers
                |> List.indexedMap
                    (\i m ->
                        m
                            |> NumberInput.view
                            |> (\e -> li [] [ e ])
                            |> Html.map (UpdateNumber i)
                    )
    in
    div []
        [ h1 [] [ text "header" ]
        , button [ onClick AddInput ] [ text "add" ]
        , ul [] numberInputs
        ]



--------------------------------------------------------------------------------
-- Main
--------------------------------------------------------------------------------


{-| We put all pieces together in the `main` function.
-}
main : Program () Model Msg
main =
    Browser.sandbox
        { init = initialModel
        , update = update
        , view = view
        }
