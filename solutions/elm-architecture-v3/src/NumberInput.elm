module NumberInput exposing (..)

import Html exposing (Html, div, input, text)
import Html.Attributes exposing (value)
import Html.Events exposing (onInput)


type alias Model =
    { number : Result String Int
    }


initialModel : Model
initialModel =
    { number = Ok 0
    }


type Msg
    = UpdateNumber String


update : Msg -> Model -> Model
update msg model =
    case msg of
        UpdateNumber newInput ->
            { model
                | number =
                    case String.toInt newInput of
                        Nothing ->
                            Err newInput

                        Just i ->
                            Ok i
            }


view : Model -> Html Msg
view model =
    let
        v =
            case model.number of
                Ok n ->
                    String.fromInt n

                Err e ->
                    e
    in
    div []
        [ input
            [ onInput (\newString -> UpdateNumber newString)
            , value v
            ]
            []
        , text
            (case model.number of
                Err _ ->
                    "invalid integer"

                Ok _ ->
                    ""
            )
        ]
