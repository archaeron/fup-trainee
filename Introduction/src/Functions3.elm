module Functions3 exposing (..)

{-| Higher order functions
-}


{-| `apply` is a function that accepts a function as input (and applies it to the remaining
input)
-}
apply : (a -> b) -> a -> b
apply f x =
    f x


{-| `twice` is a function that accepts and returns a function
-}
twice : (a -> a) -> (a -> a)
twice f x =
    f (f x)


{-| Exercise:
Write a function `thrice` that applies a function three times.-
-}
thrice : (a -> a) -> a -> a
thrice f x =
    f (twice f x)


{-| Exercise:
Write a function `compose` that accepts two functions and applies them
to a given argument in order.
-}
compose : (a -> b) -> (b -> c) -> a -> c
compose f g x =
    g (f x)


{-| Exercise:
reimplement `twice` and `thrice` with as an application of the function
`compose`.
-}
twiceByComp : (a -> a) -> a -> a
twiceByComp f =
    compose f f


thriceByComp : (a -> a) -> a -> a
thriceByComp f =
    compose f (twiceByComp f)


{-| The function `compose` is usually known as function composition. Variants
of function composition are built in Elm as `>>` (`comp1`) and `<<` (`comp2`)
repsectively.
-}
comp1 : (a -> b) -> (b -> c) -> a -> c
comp1 f g x =
    g (f x)


{-| The same in reverse order (usual order in Mathematics).
Built in as `<<` in ELM.
-}
comp2 : (b -> c) -> (a -> b) -> (a -> c)
comp2 f g x =
    f (g x)


{-| A higher order function that is often used when working with lists is the
`List.map` function. This function will apply a function to every element in
a list and return a new list containig the changed values.
-}
mapping : List Int
mapping =
    List.map (\n -> n + 1) [ 1, 2, 3 ]


{-| Exercise:
greet all friends using `friends`, `List.map` and `greeting`.
-}
friends : List String
friends =
    [ "Peter"
    , "Nina"
    , "Janosh"
    , "Reto"
    , "Adal"
    , "Sara"
    ]


greeting : String -> String
greeting person =
    "Hello " ++ person


greetFriends : List String
greetFriends =
    List.map greeting friends


{-| Another higher order function that is often used when working with lists is
the `List.filter` function. This function will apply a predicate (= boolean valued
function) to every element in a list and return a new list containig the
only the elements that satisfy the predicate.
-}
someOddNumbers : List Int
someOddNumbers =
    List.filter (\n -> modBy 2 n == 1) (List.range 1 10)


{-| Exercise:
invite all friends for dinner that are from Zurich.
Use `invite`, `peopleInZurich`, `List.member` (look it up), `friends`, `List.map` and `List.filter`.
-}
invite : String -> String
invite person =
    person ++ " you are invited for dinner!"


peopleInZurich : List String
peopleInZurich =
    [ "Peter"
    , "Reto"
    , "Sara"
    , "Jack"
    , "Adal"
    , "Milo"
    , "Sandra"
    ]


inviteFriends : List String
inviteFriends =
    friends
        |> List.filter (\p -> List.member p peopleInZurich)
        |> List.map invite


{-| `List.filter` and `List.map` are both special cases of `List.foldl` and
`List.foldr`. `List.foldl` and `List.foldl` are called left and right fold
respectively. Folding a list essentially means to traverse the given list
(starting from the right side or the left side respectively) while building
a value (by repeatedly applying a given function) that is returned once the
whole list is traversed.
-}
sumOfElements : List Int -> Int
sumOfElements =
    List.foldl (+) 0


concatStrings : List String -> String
concatStrings =
    List.foldr (++) ""


{-| Exercise:
declare a function `productOfElements` that computes the product of all elements in a list.
Use one of the folding functions.
-}
productOfElements : List Int -> Int
productOfElements =
    List.foldl (*) 1


{-| Exercise:
Use one of the folding functions to reverse a list. Use the `cons` operator `(::)`.
-}
myRev : List a -> List a
myRev =
    List.foldl (::) []


{-| Exercise:
Use one of the folding functions to implement the `List.map` function.
-}
myMap : (a -> b) -> List a -> List b
myMap f =
    List.foldr (\x xs -> f x :: xs) []


{-| Exercise:
Use one of the folding functions to implement the `List.filter` function.
-}
myFilter : (a -> Bool) -> List a -> List a
myFilter p =
    let
        f x xs =
            if p x then
                x :: xs

            else
                xs
    in
    List.foldr f []
