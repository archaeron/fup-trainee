module BaseTodo exposing (..)

{-| A short introduction to Elm and it's syntax.
-}

-- a single line comment
{- a multiline comment
   {- can be nested -}
-}
{- Generally, in Elm we define the type of a variable an a separate line before we define the variable

   # Numbers
-}


five : Int
five =
    5


four : Float
four =
    4


{-| `number` stands for either Int or Float
-}
three : number
three =
    3



{- Basic numeric operations -}


seven : Int
seven =
    five + 2


theIntEight : Int
theIntEight =
    three + five


theFloatSeven : Float
theFloatSeven =
    three + four


twentyFour : number
twentyFour =
    3 * 2 ^ 3


intSevenByThree : Int
intSevenByThree =
    seven // 3


floatSevenByThree : Float
floatSevenByThree =
    7 / 3



{-
   Exercise: Fill out the types (check your solutions with the REPL)

   x : ?
   x =
       2 ^ 3

   y : ?
   y =
       2.0 ^ 3

   z : ?
   z =
       2.0 ^ 3.0

      # Booleans
-}


true : Bool
true =
    True



{- # Strings and Chars -}


aChar : Char
aChar =
    'a'


aString : String
aString =
    "Happy new year"


greeting : String
greeting =
    aString ++ " " ++ "2021"


greeting2 : String
greeting2 =
    String.concat
        [ aString
        , " "
        , "2021"
        ]



{- Warning: The operations are strongly typed; they can only digest
   values of the same type.
   The following declarations are rejected by the type checker:

    aString = "Happy new year"
    greeting = aString ++ " " ++ 2021

    five : Int
    five = 5
    seven = five + 2.0

-}
{-
   # Lists

   We will often use `List` (linked lists),
   a generic container to hold multiple values of the same type.

-}


aListOfStrings : List String
aListOfStrings =
    [ "one", "two", "three" ]


aListOfInts : List Int
aListOfInts =
    [ 1, 2, 3 ]



{-
   Exercise: Fill out the types (check your solutions with the REPL)

   friendGroups : ?
   friendGroups =
       [ ["Peter", "Anna", "Roman", "Laura"]
       , ["Anna","Reto"]
       , ["Christoph", "Mara", "Andrew"]
       ]

-}
{- Take 15 minutes: https://package.elm-lang.org/
   try it out in the repl!
-}
