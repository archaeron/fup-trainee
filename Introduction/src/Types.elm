module Types exposing (..)

{-| Union and record types
-}


{-| We can defined a type for a record.
It is an alias, because we could just write down
the record type wherever we want, but this way
is easier.
-}
type alias Character =
    { first : String
    , last : String
    }


han : Character
han =
    { first = "Han"
    , last = "Solo"
    }


obiWan : Character
obiWan =
    { first = "Obi-Wan"
    , last = "Kenobi"
    }


{-| Values can be extracted from records through their `.field` functions.
-}
solo : String
solo =
    .first han


kenobi : String
kenobi =
    .last obiWan


{-| `.first` and `.last` can be used like any other function.
-}
firstNames : List String
firstNames =
    List.map .first [ han, obiWan ]


{-| Exercise:
Greet Obi-Wan and Han, each with a phrase like e.g. "Hello Obi-Wan!".
Use List.map and .first.

Advanced:
use `(<<)` or `(>>)`.

-}
greetings : List String
greetings =
    List.map (.first >> (\x -> "Hello " ++ x)) [ han, obiWan ]


{-| Values of a record can be changed with the syntax below.
What is important to note here is that `han` will not be changed.
A copy of `han` will be made with the first name changed.
-}
ben : Character
ben =
    { han
        | first = "Ben"
    }


{-| Here we define our first sum type. It is a type that contains exactly two values.
This type is isomorphic to `Bool`. In the type definition, the `|` can be read like an `or`.
-}
type YesNo
    = Yes
    | No


yes : YesNo
yes =
    Yes


{-| The following types look similar to the type above, but they contain
values in the options.
-}
type Identification
    = Email String
    | Username String


hanId : Identification
hanId =
    Username "han_32"


obiWanId : Identification
obiWanId =
    Email "kenobi@rebel-alliance.space"


type Shape
    = Circle { radius : Float }
    | Rectangle { width : Float, length : Float }


{-| Pattern matching is a convenient way of handling the
values of a union type. Not that the elm compiler will
check that you handle all options.
-}
changeMind : YesNo -> YesNo
changeMind opinion =
    case opinion of
        Yes ->
            No

        No ->
            Yes


isSquare : Shape -> Bool
isSquare shape =
    case shape of
        Rectangle r ->
            .length r == .width r

        Circle _ ->
            False


{-| Exercise:
Use pattern-matching to write a function `area`
that computes the area of a given shape.
Hint: There is a constant float `pi`.
-}
area : Shape -> Float
area shape =
    case shape of
        Circle c ->
            pi * .radius c ^ 2

        Rectangle r ->
            .length r * .width r


{-| A cool fact about union types is that they can be recursive.
The standard example is that of binary trees.
-}
type BinaryTree a
    = Node a (BinaryTree a) (BinaryTree a)
    | Leaf a


{-| A simple binary tree with integers.
-}
tree : BinaryTree Int
tree =
    Node 1 (Leaf 2) (Leaf 3)


{-| Values of recursive union types are very naturally
deconstructed recursively.
-}
depth : BinaryTree a -> Int
depth t =
    case t of
        Node _ l r ->
            1 + max (depth l) (depth r)

        _ ->
            1


{-| We can also use records within union types,
for example to in the definition of binary trees.
-}
type BinaryTreeR a
    = NodeR
        { payload : a
        , left : BinaryTreeR a
        , right : BinaryTreeR a
        }
    | LeafR a


treeR : BinaryTreeR Int
treeR =
    NodeR { payload = 1, left = LeafR 2, right = LeafR 3 }


{-| Exercise:
Implement the function `depth` also for `BinaryTreeR`
-}
depthR : BinaryTreeR a -> Int
depthR t =
    case t of
        NodeR n ->
            1 + max (depthR (.left n)) (depthR (.right n))

        _ ->
            1


{-| Lists are also a recursive sum type (with syntactic sugar for `Cons` and `E`)
-}
type MyList a
    = Cons { head : a, tail : MyList a }
    | E


{-| [1,2,3] as `MyList`
-}
list : MyList Int
list =
    Cons
        { head = 1
        , tail =
            Cons
                { head = 2
                , tail =
                    Cons
                        { head = 3
                        , tail = E
                        }
                }
        }


{-| Exercise:
Implement a `map` function for `myList`.

Advanced:
Implement a `fold` function for `myList`

-}
myMap : (a -> b) -> MyList a -> MyList b
myMap f xs =
    case xs of
        E ->
            E

        Cons { head, tail } ->
            Cons { head = f head, tail = myMap f tail }


myFold : (x -> y -> y) -> y -> MyList x -> y
myFold f y0 xs =
    case xs of
        E ->
            y0

        Cons { head, tail } ->
            myFold f (f head y0) tail


{-| Scott Wlaschin's (<https://fsharpforfunandprofit.com/>) example
-}
type alias CardNumber =
    String


type CardType
    = Visa
    | Amex
    | Master


type alias CardInfo =
    { cardType : CardType
    , cardNumber : CardNumber
    }


type alias Email =
    String


type PaymentMethod
    = Cash
    | Card CardInfo
    | Paypal Email


type Currency
    = EUR
    | USD
    | CHF


type alias Payment =
    { amount : Float
    , currency : Currency
    , paymentMethod : PaymentMethod
    }


aPayment : Payment
aPayment =
    { amount = 500
    , currency = CHF
    , paymentMethod =
        Card
            { cardType = Master
            , cardNumber = "123"
            }
    }
