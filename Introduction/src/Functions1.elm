module Functions1 exposing (..)

{-| On how to create simple functions

Input on the left, output on the right.

Functions also have a type: input type on the left side,
output type on the right side,
arrow inbetween.

-}


inc1 : Int -> Int
inc1 n =
    n + 1


{-| Everything can also be written on the right side (lambda notation).
In fact, `inc1` is just syntactic sugar for `inc2`
-}
inc2 : Int -> Int
inc2 =
    \n -> n + 1


{-| In order to apply a function, write the function to the left of the
input (no brackets needed).
-}
two : Int
two =
    inc1 1


greeting : String -> String
greeting person =
    "Hello " ++ person


{-| Exercise:
Define a function `square`, that squares the given input
and write down its type.
Define the same function using the lambda notation.
-}
square : number -> number
square x =
    x * x
