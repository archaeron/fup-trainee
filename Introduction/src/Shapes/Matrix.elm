module Shapes.Matrix exposing (..)


type Matrix
    = M Float Float Float Float


type alias Vector =
    ( Float, Float )


apply : Matrix -> Vector -> Vector
apply (M a b c d) ( x, y ) =
    ( a * x + b * y, c * x + d * y )


mult : Matrix -> Matrix -> Matrix
mult (M a b c d) (M r s t u) =
    M
        (a * r + b * t)
        (a * s + b * u)
        (c * r + d * t)
        (c * s + d * u)


rot : Float -> Matrix
rot a =
    M
        (cos a)
        (-1 * sin a)
        (sin a)
        (cos a)


flip : Float -> Matrix
flip angle =
    let
        a =
            2 * angle
    in
    M
        (cos a)
        (sin a)
        (sin a)
        -(cos a)


stretch : Vector -> Matrix
stretch ( x, y ) =
    M x 0 0 y
