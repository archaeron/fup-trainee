module Shapes.Shapes exposing (..)

--import Html exposing (img)

import Html.Attributes exposing (height)
import Image
import Shapes.Matrix as Matrix


type alias Point =
    ( Float, Float )


type Shape
    = Empty
    | UnitDisc
    | UnitSquare
    | Translate { dx : Float, dy : Float } Shape
    | Stretch { xFactor : Float, yFactor : Float } Shape
    | Rotate Float Shape
    | Flip { angle : Float } Shape
    | Intersect Shape Shape
    | Invert Shape


{-| Some derived Modifiers
-}
minus : Shape -> Shape -> Shape
minus s1 s2 =
    Intersect s1 (Invert s2)


disc : Float -> Shape
disc radius =
    Stretch { xFactor = radius, yFactor = radius } UnitDisc


circle : { radius : Float, stroke : Float } -> Shape
circle { radius, stroke } =
    let
        inner : Shape
        inner =
            disc (radius - stroke)

        outer : Shape
        outer =
            disc radius
    in
    minus outer inner


merge : Shape -> Shape -> Shape
merge s1 s2 =
    Invert
        (Intersect
            (Invert s1)
            (Invert s2)
        )


{-| Semantics
-}
inside : Shape -> Point -> Bool
inside s ( x, y ) =
    case s of
        Empty ->
            False

        UnitDisc ->
            x ^ 2 + y ^ 2 <= 1

        UnitSquare ->
            abs x <= 0.5 && abs y <= 0.5

        Translate { dx, dy } s1 ->
            inside s1 ( x - dx, y - dy )

        Stretch { xFactor, yFactor } s1 ->
            inside
                s1
                (Matrix.apply
                    (Matrix.stretch ( 1 / xFactor, 1 / yFactor ))
                    ( x, y )
                )

        Rotate a s1 ->
            inside
                s1
                (Matrix.apply
                    (Matrix.rot -a)
                    ( x, y )
                )

        Flip { angle } s1 ->
            inside
                s1
                (Matrix.apply
                    (Matrix.flip angle)
                    ( x, y )
                )

        Intersect s1 s2 ->
            inside s1 ( x, y ) && inside s2 ( x, y )

        Invert s1 ->
            not (inside s1 ( x, y ))


type alias Dimensions =
    { width : Int, height : Int }


makeImg : Dimensions -> Shape -> Image.Image
makeImg dim s =
    let
        ( height, width ) =
            ( .height dim, .width dim )

        points =
            height * width

        color ( x, y ) =
            if inside s ( x, y ) then
                0x0FFF

            else
                0x00B6BBCF

        mapPair f ( x, y ) =
            ( f x, f y )

        render p =
            ( modBy width p, p // height )
                |> mapPair toFloat
                |> color
    in
    List.range 1 points
        |> List.map render
        |> Image.fromList width
