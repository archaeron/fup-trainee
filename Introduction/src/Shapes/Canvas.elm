module Shapes.Canvas exposing (..)

import Shapes.Shapes exposing (Shape(..), circle, disc, merge, minus)


exampleShape : Shape
exampleShape =
    let
        s1 =
            UnitDisc
                |> Stretch { xFactor = 50, yFactor = 100 }
                |> Translate { dx = 150, dy = 150 }

        s2 =
            UnitSquare
                |> Stretch { xFactor = 90, yFactor = 180 }
                |> Rotate (pi / 3)
                |> Translate { dx = 120, dy = 170 }

        s3 =
            circle { radius = 100, stroke = 2 }
                |> Translate { dx = 180, dy = 150 }
    in
    s1
        |> minus s2
        |> merge s3
        |> Flip { angle = pi / 4 }
