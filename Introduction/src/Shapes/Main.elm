module Shapes.Main exposing (..)

import Html exposing (img)
import Html.Attributes exposing (src)
import Image
import Shapes.Canvas as Picture
import Shapes.Shapes as S


{-| `main` renders the example shape
-}
main : Html.Html msg
main =
    let
        imageData : Image.Image
        imageData =
            S.makeImg { width = 500, height = 500 } Picture.exampleShape

        pngEncodeBase64Url =
            Image.toPngUrl imageData
    in
    img [ src pngEncodeBase64Url ] []
