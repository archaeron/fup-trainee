module Recursion exposing (..)

import BigInt as I


{-| The simplest for of recursion. Counting down a natural number
-}
exp2 : Int -> Int
exp2 n =
    if n == 0 then
        1

    else
        2 * exp2 (n - 1)


{-| Remember the functions twice and thrice? We can rewrite them parametrically.
-}
nTimes n f x =
    if n == 0 then
        x

    else
        f x |> nTimes (n - 1) f



{- However, recursion is not necessarily about counting down a natural number.
   We can (try to) recurse along any function.
   Exercise:
       * Read: https://de.wikipedia.org/wiki/Collatz-Problem
       * Write a function `col` that generates the Collatz sequence for any giving initial value.
       E.g. `col 3 = [3,10,5,16,8,4,2,1]`.
-}


{-| Use this function to count "down"
-}
next : Int -> Int
next n =
    if modBy 2 n == 0 then
        n // 2

    else
        3 * n + 1


{-| Solution
-}
col : Int -> List Int
col n =
    if n == 1 then
        [ 1 ]

    else
        n :: col (next n)



{- Another way to recurse is `structural recursion`. Instead of counting down
   some natural number, some data type is further deconstructed at each recursive step.
-}


{-| We have already encountered structural recursion when working with trees and lists.
-}
type BTree a
    = Node a (BTree a) (BTree a)
    | Leaf a


{-| Similarly as with lists, we can also map over trees
-}
treeMap : (a -> b) -> BTree a -> BTree b
treeMap f t =
    case t of
        Leaf a ->
            Leaf (f a)

        Node a left right ->
            Node (f a) (treeMap f left) (treeMap f right)


{-| We can also implement some sort of "fold" functions for trees.
-}
treeFold : (a -> b -> b -> b) -> (a -> b) -> BTree a -> b
treeFold node leaf t =
    let
        f =
            treeFold node leaf
    in
    case t of
        Leaf a ->
            leaf a

        Node a l r ->
            node a (f l) (f r)



{--Exercise: use the treeFold function to implement the treeMap function.
-}


{-| Solution
-}
treeMap_ : (a -> b) -> BTree a -> BTree b
treeMap_ f =
    let
        leaf =
            Leaf << f

        node a l r =
            Node (f a) l r
    in
    treeFold node leaf


exampleT : BTree number
exampleT =
    Node 2 (Leaf 3) (Leaf 4)



{--When implementing the function `treeFold`
, we followed a very generic pattern that can 
be repeated for any (recursive) sum type. 

The procedure is as follows:
* For each constructor of the type (each case of the union), provide 
a function parameter.
* Systematically replace every constructor by the corresponding function.

We end up with a function that allows us to build anything "in the way 
or structure" that a specific tree was built. In fact, the `treeFold` 
function can be seen as a "recursion schema" tailor made for the type.
By following the same pattern, we can systematically build "recursion
 schemata" for arbitrary recursive sum types.
-}


type AExp
    = Constant Int
    | Var String
    | Sum AExp AExp
    | Prod AExp AExp


aExp :
    (Int -> b)
    -> (String -> b)
    -> (b -> b -> b)
    -> (b -> b -> b)
    -> AExp
    -> b
aExp constant var sum prod expr =
    let
        f =
            aExp constant var sum prod
    in
    case expr of
        Constant n ->
            constant n

        Var name ->
            var name

        Sum e1 e2 ->
            sum (f e1) (f e2)

        Prod e1 e2 ->
            prod (f e1) (f e2)



{- Exercise: Use the function aExp to implement the simplified `eval` function -}


eval : AExp -> Maybe Int
eval =
    let
        const =
            Just

        var _ =
            Nothing

        sum m1 m2 =
            Maybe.map2 (+) m1 m2

        prod m1 m2 =
            Maybe.map2 (*) m1 m2
    in
    aExp const var sum prod


exExpr : AExp
exExpr =
    Sum
        (Constant 2)
        (Prod
            (Constant 3)
            (Sum (Constant 1) (Constant 4))
        )


exExpr2 : AExp
exExpr2 =
    Sum (Var "x") exExpr



{- Exercise:
   Use the function aExp to implement the `pretty` function (with parentheses)
-}


{-| Solution
-}
pretty : AExp -> String
pretty =
    let
        const =
            String.fromInt

        var s =
            s

        build op arg1 arg2 =
            String.concat
                [ "("
                , arg1
                , " "
                , op
                , " "
                , arg2
                , ")"
                ]

        sum =
            build "+"

        prod =
            build "*"
    in
    aExp const var sum prod



{--Tail recursion

A function is in "tail recursive form", when all
recursive calls are the "final action" of the
respectiv branch of the function. "Final action" 
has to be undoerstood as "the last thing the 
function does" (not necessarily syntactically the
last term).
--}


{-| Not in tail recursive form
-}
exp3 n =
    if n == 0 then
        1

    else
        3 * exp3 n


{-| In tail recursive form
-}
gcd : Int -> Int -> Int
gcd x y =
    if x == 0 then
        y

    else if x > y then
        gcd y x

    else
        gcd x (y - x)



{- Exercise:
   Which ones are in tail recursive form?

   f1 g x =
   if x == 0 then
   1
   else
   f1 <| g x

   f2 g x =
   if x == 0 then
   1
   else
   g x |> f2

   f3 f n x =
   if n == 0 then
   x
   else
   f (f3 f (n-1) x)

   f4 f n x =
   if n == 0 then
   x
   else
   f4 f (n-1) (f x)

-}


{-| The function `exp3` can be written in tail recursing form
`exp3a` using a parameter/accumulator to store intermediate
results. This transformation is quite generic and is sometimes
called the "accumulator pattern".
-}
exp3a : Int -> Int
exp3a =
    let
        aux acc n =
            if n == 0 then
                acc

            else
                aux (3 * acc) (n - 1)
    in
    aux 1


{-| The nice things about tail recursion is that it can be optimized by the compiler
-}
exp3aI : Int -> I.BigInt
exp3aI =
    let
        aux acc n =
            if n == 0 then
                acc

            else
                aux (I.mul (I.fromInt 3) acc) (n - 1)
    in
    aux (I.fromInt 1)


{-| Sometimes the accumulator can be a bit more complex than
just a number.
-}
fibs : Int -> I.BigInt
fibs =
    let
        aux a b n =
            if n == 0 then
                a

            else
                aux (I.add a b) a (n - 1)
    in
    aux (I.fromInt 1) (I.fromInt 0)



{- The accumulator can also be a function -}


exp3c : Int -> Int
exp3c =
    let
        aux f n =
            if n <= 0 then
                f ()

            else
                aux (\x -> 3 * f x) (n - 1)
    in
    aux (\_ -> 1)


{-| A tail recursive implementation of the `map` function for lists
using a list as accumulator
-}
myMapA : (a -> b) -> List a -> List b
myMapA f =
    let
        aux acc xs =
            case xs of
                [] ->
                    acc

                x :: xs_ ->
                    aux (f x :: acc) xs_
    in
    List.reverse << aux []



{- Exercise:
   Use a function as an accumulator to implement a tailrecursive
   map functions for lists. Your implementation should

     - Not traverse the list at every recursive call
     - Not need to reverse any list explicitly

   myMapC : (a -> b) -> List a -> List b
   myMapC f =
   let
   aux ctn xs =
   case xs of
   [] ->
   ctn []

                   x :: xs_ ->
                       aux (\a -> ctn (f x :: xs_)) xs_
       in
       aux (\_ -> [])

-}
