module Functions2 exposing (..)

{-| Function of several variables
-}


{-| `addTupled` is binary function, its output depends on two separate input variables.
-}
addTupled : ( Int, Int ) -> Int
addTupled ( x, y ) =
    x + y


{-| `avg3Tupled` is a function that depends on three input variables.
-}
avg3Tupled : ( Float, Float, Float ) -> Float
avg3Tupled ( x, y, z ) =
    (x + y + z) / 3


{-| We can evaluate a function of several variables by providing values for each input variabele.
-}
four : Float
four =
    avg3Tupled ( 3, 4, 5 )



{- If we want to evaluate such a multivariable function before we have all inputs
   ready, we have to properly parametrize it. We can do this with curried functions.
-}


{-| `add` is the parametrized (a.k.a curried) version of `add`.
It is a function that returns a (unary) function.

Note that the following three lines are equivalent declarations:

`add = \x -> \y -> x + y`

`add x = \y -> x + y`

`add x y = x + y`

Also note that the parenthesis are not needed in the declaration
of the type of `add`.

-}
add : Int -> (Int -> Int)
add n m =
    n + m


{-| `add3` is the function that adds `3` to any given input.
Thanks to currying, we can realize `add3` as a special
case of `add`. Thanks to partial application, this corresponds
to a single function call.
-}
add3 : Int -> Int
add3 =
    add 3


{-| Exercise:
Declare a curried version of the function `avg3Tupled` as a lambda term.
-}
avg3 : Float -> Float -> Float -> Float
avg3 =
    \x -> \y -> \z -> (x + y + z) / 3


{-| Exercise:
use the binary function `(++)` that concatenates strings to specify
a function that prepends "=> " to any given input string. Use partial application
-}
prepArrow : String -> String
prepArrow =
    (++) "=> "
