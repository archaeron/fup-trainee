module Optional exposing (..)

import Maybe exposing (andThen)


{-| The `Optional` type is built into Elm as
`type Maybe a = Just a | Nothing`.
The `Maybe` type is Elm's (and Haskell's, Scala's, ...) answer
to `null`.
Unlike most mainstream languages we must clearly mark which values
can be "`null`".
-}



{- Todo parallel Either -}
{- An `Optional a` represents a value of type `a` that can be absent. -}


type Optional a
    = None
    | Some a


{-| Some Int
-}
theInt : Optional Int
theInt =
    Some 5



{- Sometimes we want to be a bit more specific with absent values
   (e.g. error messages)
-}


type Either a b
    = Left a
    | Right b


{-| It is customary to use the `Right` parameter in the `Either`
type to denot the "good" or ecpected type - as opposed to e.g. an
error message.
-}
success5 : Either String Int
success5 =
    Right 5


someError : Either String Int
someError =
    Left "Something went wrong"


{-| Integer division without surprises
-}
divide : Int -> Int -> Optional Int
divide x y =
    case y of
        0 ->
            None

        _ ->
            Some (x // y)


{-| Exercise:
complete the function `divideE`.
-}
divideE : Int -> Int -> Either String Int
divideE =
    Debug.todo "implement divideE!"


{-| Partial application still works as expected
-}
divide42 : Int -> Optional Int
divide42 =
    divide 42


divide233 : Int -> Optional Int
divide233 =
    divide 233


{-| Composition also still works
-}
addThenDivide : Int -> Optional Int
addThenDivide =
    (+) 3 >> divide233



{- Most of the time. However, `divideThenAdd` will not work without a fix.
   divideThenAdd =
       divide233 >> (+) 3


-}


{-| We can fix the code as follows, but the solution is not generic at all.
We can gereralize the solution.
-}
divideThenAddHard : Int -> Optional Int
divideThenAddHard x =
    case divide233 x of
        None ->
            None

        Some y ->
            Some (y + 3)


{-| The general solution is `map` for our `Optional`.
Apply a function to the value if it exists.
-}
map : (a -> b) -> Optional a -> Optional b
map f mx =
    case mx of
        Some x ->
            Some (f x)

        None ->
            None


{-| Exercise:
Write a map function for Either.
-}
mapE : (a -> b) -> Either c a -> Either c b
mapE =
    Debug.todo "implement mapE"


{-| Now we can use the map function to divide and then add
-}
divideThenAdd : Int -> Optional Int
divideThenAdd x =
    x |> divide233 |> map ((+) 3)


{-| The situation is similar if we have two place functions. For example
if we want to add any number instead of fixed 3. Again, let's have a look
ath the the "ugly", not generic apporach first.
-}
divideThenAdd2Ugly : Int -> Int -> Optional Int
divideThenAdd2Ugly x y =
    case divide233 x of
        None ->
            None

        Some x1 ->
            case divide42 y of
                None ->
                    None

                Some y1 ->
                    Some (x1 + y1)


{-| The generic pattern to use here is called `apply`, we can later use it
to derive a version of map for two place functions.
-}
apply : Optional (a -> b) -> Optional a -> Optional b
apply mf ma =
    case mf of
        Some f ->
            map f ma

        None ->
            None


{-| `map2` can be used to map two place functions.
-}
map2 : (a -> b -> c) -> Optional a -> Optional b -> Optional c
map2 f ma mb =
    apply (map f ma) mb


{-| Exercise:
Use the function `apply` to implement the three place map.
-}
map3 :
    (a -> b -> c -> d)
    -> Optional a
    -> Optional b
    -> Optional c
    -> Optional d
map3 f ma mb mc =
    apply (apply (map f ma) mb) mc


{-| Now we can divide and then add without hassle.
-}
divideThenAdd2 : Int -> Int -> Optional Int
divideThenAdd2 x y =
    map2 (+) (divide233 x) (divide42 y)


{-| Exercise:
Define `apply` and map2 for Either.
-}
applyE : Either l (a -> b) -> Either l a -> Either l b
applyE ef ex =
    case ef of
        Right f ->
            mapE f ex

        Left l ->
            Left l


map2E : (a -> b -> c) -> Either l a -> Either l b -> Either l c
map2E f ex ey =
    applyE (mapE f ex) ey


{-| Finally, there is also a pattern to deal with chaining several functions that return Optional values.
`andThen` might be familiar from `Promise.then` in JavaScript.
-}
andThen : (a -> Optional b) -> Optional a -> Optional b
andThen f m =
    case m of
        Some x ->
            f x

        None ->
            None


{-| `andThen` will help us to avoid code like the following.
-}
ugly : (a -> Optional b) -> (b -> Optional c) -> (c -> Optional d) -> a -> Optional d
ugly f1 f2 f3 x1 =
    case f1 x1 of
        None ->
            None

        Some x2 ->
            case f2 x2 of
                None ->
                    None

                Some x3 ->
                    f3 x3


{-| And instead write it nicer
-}
nice : (a -> Optional b) -> (b -> Optional c) -> (c -> Optional d) -> a -> Optional d
nice f1 f2 f3 x1 =
    x1
        |> f1
        |> andThen f2
        |> andThen f3
