module Expense.Json exposing (..)

import Expense.Types
    exposing
        ( Expense
        , Expenses
        , FeatureSelection
        )
import Html exposing (option)
import Json.Decode as Decode
import Json.Encode as Encode



--------------------------------------------------------------------------------
-- Decoders
--------------------------------------------------------------------------------


featureSelectionDecoder : Decode.Decoder FeatureSelection
featureSelectionDecoder =
    Decode.map2
        (\feature option ->
            { feature = feature
            , option = option
            }
        )
        (Decode.field "feature" Decode.string)
        (Decode.field "option" Decode.string)


featureSelectionsDecoder : Decode.Decoder (List FeatureSelection)
featureSelectionsDecoder =
    Decode.list featureSelectionDecoder


expenseDecoder : Decode.Decoder Expense
expenseDecoder =
    Decode.map3
        (\amount description features ->
            { amount = amount
            , description = description
            , features = features
            }
        )
        (Decode.field "amount" Decode.float)
        (Decode.field "description" Decode.string)
        (Decode.field "features" featureSelectionsDecoder)


expensesDecoder : Decode.Decoder Expenses
expensesDecoder =
    Decode.list expenseDecoder



--------------------------------------------------
-- Encoders
--------------------------------------------------


featureSelectionEncoder : FeatureSelection -> Encode.Value
featureSelectionEncoder { feature, option } =
    Encode.object
        [ ( "feature", Encode.string feature )
        , ( "option", Encode.string option )
        ]


featureSelectionsEncoder : List FeatureSelection -> Encode.Value
featureSelectionsEncoder =
    Encode.list featureSelectionEncoder


expenseEncoder : Expense -> Encode.Value
expenseEncoder { amount, description, features } =
    Encode.object
        [ ( "description", Encode.string description )
        , ( "amount", Encode.float amount )
        , ( "features", featureSelectionsEncoder features )
        ]
