module Expense.Types exposing
    ( Expense
    , Expenses
    , FeatureSelection
    )


type alias FeatureSelection =
    { feature : String
    , option : String
    }


type alias Expense =
    { amount : Float
    , description : String
    , features : List FeatureSelection
    }


type alias Expenses =
    List Expense
