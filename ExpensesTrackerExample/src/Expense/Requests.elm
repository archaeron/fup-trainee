module Expense.Requests exposing (..)

import Expense.Json
    exposing
        ( expenseDecoder
        , expenseEncoder
        , expensesDecoder
        )
import Expense.Types exposing (Expense, Expenses)
import Http


getExpenses : Cmd (Result Http.Error Expenses)
getExpenses =
    Http.get
        { url = "http://localhost:3000/expense"
        , expect =
            Http.expectJson identity expensesDecoder
        }


postExpense : Expense -> Cmd (Result Http.Error Expense)
postExpense expense =
    Http.post
        { url = "http://localhost:3000/expense"
        , body =
            Http.jsonBody (expenseEncoder expense)
        , expect =
            Http.expectJson identity expenseDecoder
        }
