module Feature.Requests exposing (..)

import Feature.Json
    exposing
        ( featureDecoder
        , featureEncoder
        , featuresDecoder
        )
import Feature.Types exposing (Feature, Features)
import Http


getFeature : String -> Cmd (Result Http.Error Feature)
getFeature name =
    Http.get
        { url = "http://localhost:3000/feature/" ++ name
        , expect =
            Http.expectJson identity featureDecoder
        }


getFeatures : Cmd (Result Http.Error Features)
getFeatures =
    Http.get
        { url = "http://localhost:3000/feature"
        , expect =
            Http.expectJson identity featuresDecoder
        }


postFeature : Feature -> Cmd (Result Http.Error Feature)
postFeature feature =
    Http.post
        { url = "http://localhost:3000/feature"
        , body =
            Http.jsonBody (featureEncoder feature)
        , expect =
            Http.expectJson identity featureDecoder
        }
