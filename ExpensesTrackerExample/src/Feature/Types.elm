module Feature.Types exposing
    ( Feature
    , FeatureName
    , Features
    )

import Dict
import Set as S


type alias FeatureName =
    String


type alias Feature =
    { name : FeatureName
    , options : S.Set String
    }


type alias Features =
    Dict.Dict FeatureName Feature
