module Feature.Json exposing (..)

import Feature.Types exposing (Feature, Features)
import Json.Decode as Decode
import Json.Encode as Encode
import Set as S



--------------------------------------------------
-- Decoders
--------------------------------------------------


optionsDecoder : Decode.Decoder (S.Set String)
optionsDecoder =
    Decode.map S.fromList (Decode.list Decode.string)


featureDecoder : Decode.Decoder Feature
featureDecoder =
    Decode.map2
        (\name options ->
            { name = name, options = options }
        )
        (Decode.field "name" Decode.string)
        (Decode.field "options" optionsDecoder)


featuresDecoder : Decode.Decoder Features
featuresDecoder =
    Decode.dict featureDecoder



--------------------------------------------------
-- Encoders
--------------------------------------------------


optionsEncoder : S.Set String -> Encode.Value
optionsEncoder options =
    Encode.list Encode.string (S.toList options)


featureEncoder : Feature -> Encode.Value
featureEncoder { name, options } =
    Encode.object
        [ ( "name", Encode.string name )
        , ( "options", optionsEncoder options )
        ]
