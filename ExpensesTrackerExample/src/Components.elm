module Components exposing
    ( badge
    , badgeWithX
    , button
    , input
    , labeled
    , numberInput
    , select
    , spinner
    )

import Html
import Html.Attributes as Attr
import Html.Events as Events exposing (onInput)


button :
    { onPress : msg
    , label : String
    }
    -> Html.Html msg
button { label, onPress } =
    Html.button
        [ Attr.class "btn btn-primary"
        , Events.onClick onPress
        ]
        [ Html.text label
        ]


input :
    { placeholder : String
    , text : String
    , onChange : String -> msg
    }
    -> Html.Html msg
input { placeholder, text, onChange } =
    Html.input
        [ Attr.class "form-control"
        , Attr.placeholder placeholder
        , Attr.value text
        , Events.onInput onChange
        ]
        []


numberInput :
    { placeholder : String
    , value : Result String Float
    , onChange : Result String Float -> msg
    }
    -> Html.Html msg
numberInput { placeholder, value, onChange } =
    let
        ( text, inputClass, errorView ) =
            case value of
                Ok f ->
                    ( String.fromFloat f
                    , "form-control is-valid"
                    , Html.span [] []
                    )

                Err s ->
                    ( s
                    , "form-control is-invalid"
                    , Html.span [ Attr.class "invalid-feedback" ] [ Html.text "Could not parse float" ]
                    )
    in
    Html.div
        []
        [ Html.input
            [ Attr.class inputClass
            , Attr.placeholder placeholder
            , Attr.value text
            , Attr.type_ "number"
            , Events.onInput
                (\i ->
                    case String.toFloat i of
                        Nothing ->
                            onChange (Err i)

                        Just f ->
                            onChange (Ok f)
                )
            ]
            []
        , errorView
        ]


{-| A loading spinner.
-}
spinner : Html.Html msg
spinner =
    Html.div
        [ Attr.class "spinner-border"
        ]
        [ Html.span
            [ Attr.class "visually-hidden"
            ]
            [ Html.text "Loading..."
            ]
        ]


labeled : String -> List (Html.Html msg) -> Html.Html msg
labeled label contents =
    Html.div
        [ Attr.class "mb-3"
        ]
        (Html.label
            [ Attr.class "form-label"
            ]
            [ Html.text label
            ]
            :: contents
        )


badge : String -> Html.Html msg
badge label =
    Html.span
        [ Attr.class "btn btn-info"
        ]
        [ Html.text label
        ]


badgeWithX :
    { onDelete : msg
    , label : String
    }
    -> Html.Html msg
badgeWithX { onDelete, label } =
    Html.span
        [ Attr.class "btn btn-info"
        , Attr.style "align-items" "center"
        , Attr.style "justify-content" "center"
        , Attr.style "display" "inline-flex"
        , Attr.style "width" "max-content"
        ]
        [ Html.text label
        , Html.button
            [ Attr.type_ "button"
            , Attr.class "btn-close btn-sm"
            , Attr.style "align-items" "center"
            , Events.onClick onDelete
            ]
            []
        ]


select :
    { elements : List a
    , noSelectionText : String
    , toLabel : a -> String
    , selected : Maybe a
    , onSelect : Maybe a -> msg
    }
    -> Html.Html msg
select { elements, noSelectionText, toLabel, selected, onSelect } =
    let
        option label =
            Html.option [] [ Html.text label ]

        noSelectionSelected =
            case selected of
                Nothing ->
                    True

                Just _ ->
                    False

        noSelection =
            Html.option
                [ Attr.disabled True, Attr.selected noSelectionSelected ]
                [ Html.text noSelectionText ]

        onInput : String -> msg
        onInput i =
            case List.filter (\( e, l ) -> l == i) (List.map (\e -> ( e, toLabel e )) elements) of
                ( e, _ ) :: _ ->
                    onSelect (Just e)

                _ ->
                    onSelect Nothing
    in
    Html.select
        [ Events.onInput onInput
        ]
        (noSelection :: List.map (toLabel >> option) elements)
