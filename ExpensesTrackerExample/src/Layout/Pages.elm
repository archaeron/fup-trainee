module Layout.Pages exposing (Page(..))

import Pages.Expenses as Expenses
import Pages.Features as Features


type Page
    = StartPage
    | FeaturesPage Features.Model
    | ExpensesPage Expenses.Model
    | NotFoundPage
