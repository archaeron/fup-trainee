module Layout.Header exposing (..)

import Layout.Pages as Page
import Route as Route
import Html
import Html.Attributes as Attrs


header : Page.Page -> Html.Html msg
header page =
    Html.nav
        [ Attrs.class "navbar navbar-expand-md navbar-dark bg-dark" ]
        [ Html.div
            [ Attrs.class "container-fluid" ]
            [ Html.a
                [ Attrs.class "navbar-brand"
                , Attrs.href "#"
                ]
                [ Html.text "Expenses Tracker" ]
            , Html.div
                [ Attrs.class "collapse navbar-collapse"
                , Attrs.id "navbarsExampleDefault"
                ]
                [ Html.ul
                    [ Attrs.class "navbar-nav me-auto mb-2 mb-md-0" ]
                    [ Html.li
                        [ Attrs.class "nav-item active" ]
                        [ Html.a
                            [ Attrs.attribute "aria-current" "page"
                            , Attrs.class "nav-link"
                            , Route.href Route.Expenses
                            ]
                            [ Html.text "Expenses" ]
                        ]
                    , Html.li
                        [ Attrs.class "nav-item" ]
                        [ Html.a
                            [ Attrs.class "nav-link"
                            , Route.href Route.ListFeatures
                            ]
                            [ Html.text "Features" ]
                        ]
                    ]
                ]
            ]
        ]
