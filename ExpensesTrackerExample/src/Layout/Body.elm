module Layout.Body exposing (..)

import Components as C
import Html
import Html.Attributes as Attrs


body : Html.Html msg -> Html.Html msg
body html =
    Html.main_
        [ Attrs.class "container"
        ]
        [ Html.div
            [ Attrs.class "py-4 px-3"
            ]
            [ html
            ]
        ]
