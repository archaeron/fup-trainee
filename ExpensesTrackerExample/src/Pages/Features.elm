module Pages.Features exposing (..)

import Dict
import Components as C
import Feature.Requests as Requests
import Feature.Types exposing (Feature, FeatureName, Features)
import Pages.Features.EditFeature as Edit
import Html
import Html.Attributes as Attrs
import Http
import Set as S



--------------------------------------------------------------------------------
-- Model
--------------------------------------------------------------------------------


type alias Model =
    { features : Maybe Features
    , requestError : Maybe Http.Error
    , editFeature : Edit.Model
    }


initialModel : Model
initialModel =
    { features = Nothing
    , requestError = Nothing
    , editFeature = Edit.initialModel
    }



--------------------------------------------------------------------------------
-- Update
--------------------------------------------------------------------------------


type Msg
    = GotFeatures (Result Http.Error Features)
    | SavedFeature (Result Http.Error Feature)
    | EditFeature Edit.Msg


init : ( Model, Cmd Msg )
init =
    ( initialModel
    , Requests.getFeatures |> Cmd.map GotFeatures
    )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        GotFeatures errorOrFeatures ->
            case errorOrFeatures of
                Ok loadedFeatures ->
                    ( { model
                        | features = Just loadedFeatures
                        , requestError = Nothing
                      }
                    , Cmd.none
                    )

                Err err ->
                    ( { model | requestError = Just err }
                    , Cmd.none
                    )

        SavedFeature errorOrFeature ->
            case errorOrFeature of
                Ok _ ->
                    ( { model | requestError = Nothing }
                    , Requests.getFeatures |> Cmd.map GotFeatures
                    )

                Err err ->
                    ( { model | requestError = Just err }
                    , Cmd.none
                    )

        EditFeature editFeatureMsg ->
            let
                editUpdated =
                    Edit.update editFeatureMsg model.editFeature
            in
            case editUpdated.savedFeature of
                Nothing ->
                    ( { model | editFeature = editUpdated.model }
                    , Cmd.none
                    )

                Just newFeature ->
                    ( { model | editFeature = editUpdated.model }
                    , Cmd.batch
                        [ newFeature
                            |> Requests.postFeature
                            |> Cmd.map SavedFeature
                        ]
                    )



--------------------------------------------------------------------------------
-- View
--------------------------------------------------------------------------------


errorToString : Http.Error -> String
errorToString err =
    case err of
        Http.BadUrl string ->
            "Invalid url: " ++ string

        Http.Timeout ->
            "Timeout while calling the server"

        Http.NetworkError ->
            "Network Error"

        Http.BadStatus statusCode ->
            "We got a bad status code: " ++ String.fromInt statusCode

        Http.BadBody body ->
            "We couldn't parse the response: " ++ body


viewError : Maybe Http.Error -> Html.Html msg
viewError maybeErr =
    case maybeErr of
        Nothing ->
            Html.div [] []

        Just err ->
            Html.div []
                [ Html.div [] [ Html.text "got an error while connecting to the server" ]
                , Html.div [] [ Html.text (errorToString err) ]
                ]


viewOption : String -> Html.Html msg
viewOption opt =
    Html.span
        [ Attrs.class "me-2"
        ]
        [ C.badge opt
        ]


viewFeature : ( FeatureName, Feature ) -> Html.Html Msg
viewFeature ( featureName, { options } ) =
    Html.div []
        [ Html.h3 [] [ Html.text featureName ]
        , Html.div
            []
            (List.map viewOption (S.toList options))
        ]


viewFeatures : Maybe Features -> Html.Html Msg
viewFeatures maybeFeatures =
    case maybeFeatures of
        Nothing ->
            Html.div
                []
                [ C.spinner
                ]

        Just features ->
            if Dict.isEmpty features then
                Html.div []
                    [ Html.text "No Features yet, create a new one!"
                    ]

            else
                Html.div [] (features |> Dict.toList |> List.map viewFeature)


view : Model -> Html.Html Msg
view { features, requestError, editFeature } =
    Html.div []
        [ Html.h1 [] [ Html.text "Features" ]
        , viewError requestError
        , viewFeatures features
        , Edit.view editFeature |> Html.map EditFeature
        ]
