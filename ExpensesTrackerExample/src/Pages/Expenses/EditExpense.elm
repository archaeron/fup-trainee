module Pages.Expenses.EditExpense exposing (..)

import Dict
import Components as C
import Expense.Types exposing (Expense, FeatureSelection)
import Feature.Types as Features
import Html exposing (option)
import Html.Attributes as Attr
import Set as S



--------------------------------------------------------------------------------
-- Model
--------------------------------------------------------------------------------


type alias Model =
    { description : String
    , amount : Result String Float
    , currentFeatureName : Maybe String
    , currentOption : Maybe String
    , featureSelections : List FeatureSelection
    }


initialModel : Model
initialModel =
    { description = ""
    , amount = Ok 0
    , currentFeatureName = Nothing
    , currentOption = Nothing
    , featureSelections = []
    }



--------------------------------------------------------------------------------
-- Update
--------------------------------------------------------------------------------


type Msg
    = ChangeDescription String
    | ChangeAmount (Result String Float)
    | ChangeCurrentFeatureName (Maybe String)
    | ChangeCurrentOption (Maybe String)
    | AddFeature
    | DeleteFeature FeatureSelection
    | SaveExpense


update : Msg -> Model -> { model : Model, savedExpense : Maybe Expense }
update msg model =
    case msg of
        ChangeDescription description ->
            { model = { model | description = description }
            , savedExpense = Nothing
            }

        ChangeAmount amountResult ->
            { model = { model | amount = amountResult }
            , savedExpense = Nothing
            }

        ChangeCurrentFeatureName maybeFeatureSelection ->
            { model =
                { model
                    | currentFeatureName = maybeFeatureSelection
                }
            , savedExpense = Nothing
            }

        ChangeCurrentOption maybeOptions ->
            { model =
                { model
                    | currentOption = maybeOptions
                }
            , savedExpense = Nothing
            }

        AddFeature ->
            { model =
                { model
                    | currentFeatureName = Nothing
                    , currentOption = Nothing
                    , featureSelections =
                        case ( model.currentFeatureName, model.currentOption ) of
                            ( Just featureName, Just option ) ->
                                { feature = featureName
                                , option = option
                                }
                                    :: model.featureSelections

                            _ ->
                                model.featureSelections
                }
            , savedExpense = Nothing
            }

        DeleteFeature selection ->
            { model =
                { model
                    | featureSelections =
                        List.filter
                            (\s -> s /= selection)
                            model.featureSelections
                }
            , savedExpense = Nothing
            }

        SaveExpense ->
            { model = model
            , savedExpense =
                case model.amount of
                    Ok amount ->
                        Just
                            { description = model.description
                            , amount = amount
                            , features = model.featureSelections
                            }

                    Err _ ->
                        Nothing
            }



--------------------------------------------------------------------------------
-- View
--------------------------------------------------------------------------------


viewFeature : FeatureSelection -> Html.Html Msg
viewFeature { feature, option } =
    Html.span
        [ Attr.class "me-2" ]
        [ C.badgeWithX
            { label = feature ++ "." ++ option
            , onDelete =
                DeleteFeature
                    { feature = feature
                    , option = option
                    }
            }
        ]


viewAmount : Result String Float -> Html.Html Msg
viewAmount amountResult =
    C.labeled "Amount"
        [ C.numberInput
            { placeholder = "Amount"
            , value = amountResult
            , onChange = ChangeAmount
            }
        ]


viewFeatureSelect :
    { feature : Maybe String
    , features : Features.Features
    , option : Maybe String
    }
    -> Html.Html Msg
viewFeatureSelect { feature, features, option } =
    Html.div []
        [ C.labeled "Feature"
            [ C.select
                { elements = Dict.keys features
                , noSelectionText = "Select a feature"
                , selected = feature
                , toLabel = identity
                , onSelect = ChangeCurrentFeatureName
                }
            ]
        , case feature of
            Nothing ->
                Html.span [] []

            Just featureName ->
                C.labeled "Option"
                    [ C.select
                        { elements =
                            case Dict.get featureName features of
                                Just f ->
                                    S.toList f.options

                                Nothing ->
                                    []
                        , noSelectionText = "Select an option"
                        , selected = option
                        , toLabel = identity
                        , onSelect = ChangeCurrentOption
                        }
                    ]
        , case ( feature, option ) of
            ( Just _, Just _ ) ->
                C.button
                    { label = "Add Feature"
                    , onPress = AddFeature
                    }

            _ ->
                Html.span [] []
        ]


view :
    Features.Features
    -> Model
    -> Html.Html Msg
view features model =
    Html.div
        []
        [ Html.h1 []
            [ Html.text "Create Expense" ]
        , C.labeled "Expense Description"
            [ C.input
                { placeholder = "Expense Description"
                , text = model.description
                , onChange = ChangeDescription
                }
            ]
        , viewAmount model.amount
        , Html.div [] (List.map viewFeature model.featureSelections)
        , viewFeatureSelect
            { feature = model.currentFeatureName
            , features = features
            , option = model.currentOption
            }
        , C.button
            { label = "Save Expense"
            , onPress = SaveExpense
            }
        ]
