module Pages.Expenses exposing (..)

import Components as C
import Expense.Requests as Requests
import Expense.Types as Expenses
    exposing
        ( Expense
        , Expenses
        )
import Feature.Requests as Features
import Feature.Types as Features
import Pages.Expenses.EditExpense as Edit
import Html exposing (option)
import Http



--------------------------------------------------------------------------------
-- Model
--------------------------------------------------------------------------------


type alias Model =
    { expenses : Maybe Expenses.Expenses
    , features : Maybe Features.Features
    , requestError : Maybe Http.Error
    , editExpense : Edit.Model
    }


initialModel : Model
initialModel =
    { expenses = Nothing
    , features = Nothing
    , requestError = Nothing
    , editExpense = Edit.initialModel
    }



--------------------------------------------------------------------------------
-- Update
--------------------------------------------------------------------------------


type Msg
    = GotExpenses (Result Http.Error Expenses)
    | GotFeatures (Result Http.Error Features.Features)
    | SavedExpense (Result Http.Error Expense)
    | EditExpense Edit.Msg
    | NoOp


init : ( Model, Cmd Msg )
init =
    ( initialModel
    , Cmd.batch
        [ Requests.getExpenses |> Cmd.map GotExpenses
        , Features.getFeatures |> Cmd.map GotFeatures
        ]
    )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        GotExpenses errorOrExpenses ->
            case errorOrExpenses of
                Ok loadedExpenses ->
                    ( { model
                        | expenses = Just loadedExpenses
                        , requestError = Nothing
                      }
                    , Cmd.none
                    )

                Err err ->
                    ( { model | requestError = Just err }
                    , Cmd.none
                    )

        GotFeatures errorOrFeatures ->
            case errorOrFeatures of
                Ok loadedFeatures ->
                    ( { model
                        | features = Just loadedFeatures
                        , requestError = Nothing
                      }
                    , Cmd.none
                    )

                Err err ->
                    ( { model | requestError = Just err }
                    , Cmd.none
                    )

        SavedExpense errorOrExpense ->
            case errorOrExpense of
                Ok _ ->
                    ( { model | requestError = Nothing }
                    , Requests.getExpenses |> Cmd.map GotExpenses
                    )

                Err err ->
                    ( { model | requestError = Just err }
                    , Cmd.none
                    )

        EditExpense editExpenseMsg ->
            let
                editUpdated =
                    Edit.update editExpenseMsg model.editExpense
            in
            case editUpdated.savedExpense of
                Nothing ->
                    ( { model | editExpense = editUpdated.model }
                    , Cmd.none
                    )

                Just newExpense ->
                    ( { model | editExpense = editUpdated.model }
                    , Cmd.batch
                        [ newExpense
                            |> Requests.postExpense
                            |> Cmd.map SavedExpense
                        ]
                    )



--------------------------------------------------------------------------------
-- View
--------------------------------------------------------------------------------


errorToString : Http.Error -> String
errorToString err =
    case err of
        Http.BadUrl string ->
            "Invalid url: " ++ string

        Http.Timeout ->
            "Timeout while calling the server"

        Http.NetworkError ->
            "Network Error"

        Http.BadStatus statusCode ->
            "We got a bad status code: " ++ String.fromInt statusCode

        Http.BadBody body ->
            "We couldn't parse the response: " ++ body


viewError : Maybe Http.Error -> Html.Html msg
viewError maybeErr =
    case maybeErr of
        Nothing ->
            Html.div [] []

        Just err ->
            Html.div []
                [ Html.div [] [ Html.text "got an error while connecting to the server" ]
                , Html.div [] [ Html.text (errorToString err) ]
                ]


viewExpense : Expense -> Html.Html Msg
viewExpense { description, amount, features } =
    Html.div
        []
        [ Html.h3 [] [ Html.text description ]
        , Html.text (String.fromFloat amount)
        , Html.ul
            []
            (List.map
                (\{ feature, option } ->
                    Html.li [] [ Html.text (feature ++ "." ++ option) ]
                )
                features
            )
        ]


viewExpenses : Maybe Expenses -> Html.Html Msg
viewExpenses maybeExpenses =
    case maybeExpenses of
        Nothing ->
            Html.div
                []
                [ C.spinner
                ]

        Just expenses ->
            if List.isEmpty expenses then
                Html.div []
                    [ Html.text "No Expenses yet, create a new one!"
                    ]

            else
                Html.div [] (expenses |> List.map viewExpense)


view : Model -> Html.Html Msg
view { expenses, features, requestError, editExpense } =
    Html.div
        []
        [ Html.h1 [] [ Html.text "Expenses" ]
        , viewError requestError
        , viewExpenses expenses
        , case features of
            Just f ->
                Edit.view f editExpense |> Html.map EditExpense

            Nothing ->
                C.spinner
        ]
