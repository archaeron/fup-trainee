module Pages.Features.EditFeature exposing (..)

import Components as C
import Feature.Types as Types exposing (Feature)
import Html
import Html.Attributes as Attr
import Set as S



--------------------------------------------------------------------------------
-- Model
--------------------------------------------------------------------------------


type alias Model =
    { name : Types.FeatureName
    , currentTag : String
    , tags : S.Set String
    }


initialModel : Model
initialModel =
    { name = ""
    , currentTag = ""
    , tags = S.empty
    }



--------------------------------------------------------------------------------
-- Update
--------------------------------------------------------------------------------


type Msg
    = ChangeName String
    | ChangeCurrentTag String
    | AddTag
    | DeleteOption String
    | SaveFeature


update : Msg -> Model -> { model : Model, savedFeature : Maybe Feature }
update msg model =
    case msg of
        ChangeName name ->
            { model = { model | name = name }
            , savedFeature = Nothing
            }

        ChangeCurrentTag tagName ->
            { model = { model | currentTag = tagName }
            , savedFeature = Nothing
            }

        AddTag ->
            { model =
                { model
                    | currentTag = ""
                    , tags =
                        case model.currentTag of
                            "" ->
                                model.tags

                            currentTag ->
                                S.insert currentTag model.tags
                }
            , savedFeature = Nothing
            }

        DeleteOption s ->
            { model = { model | tags = S.remove s model.tags }
            , savedFeature = Nothing
            }

        SaveFeature ->
            { model = model
            , savedFeature =
                Just
                    { name = model.name
                    , options = model.tags
                    }
            }



--------------------------------------------------------------------------------
-- View
--------------------------------------------------------------------------------


viewOption : String -> Html.Html Msg
viewOption s =
    Html.span
        [ Attr.class "me-2" ]
        [ C.badgeWithX
            { label = s
            , onDelete = DeleteOption s
            }
        ]


view :
    Model
    -> Html.Html Msg
view model =
    Html.div
        []
        [ Html.h1 []
            [ Html.text "Create Feature" ]
        , C.labeled "Feature Name"
            [ C.input
                { placeholder = "Feature Name"
                , text = model.name
                , onChange = ChangeName
                }
            ]
        , Html.div [] (List.map viewOption (S.toList model.tags))
        , C.labeled "New Tag"
            [ C.input
                { placeholder = "New Tag"
                , text = model.currentTag
                , onChange = ChangeCurrentTag
                }
            , C.button
                { label = "Add Tag"
                , onPress = AddTag
                }
            ]
        , C.button
            { label = "Save Feature"
            , onPress = SaveFeature
            }
        ]
