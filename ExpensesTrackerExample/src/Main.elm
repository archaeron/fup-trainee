module Main exposing (main)

import Browser
import Browser.Navigation as Nav
import Layout.Body as Body
import Layout.Header as Header
import Layout.Pages exposing (Page(..))
import Pages.Expenses as Expenses
import Pages.Features as Features
import Route as Route exposing (Route(..))
import Html
import Url exposing (Url)


type alias Model =
    { navKey : Nav.Key
    , page : Page
    }


initialModel : Nav.Key -> Model
initialModel key =
    { navKey = key
    , page = StartPage
    }


type Msg
    = FeaturesMsg Features.Msg
    | ExpensesMsg Expenses.Msg
    | ChangedUrl Url
    | ClickedLink Browser.UrlRequest


changeRoute : Maybe Route.Route -> Model -> ( Model, Cmd Msg )
changeRoute maybeRoute model =
    case maybeRoute of
        Nothing ->
            ( { model | page = NotFoundPage }
            , Cmd.none
            )

        Just Route.Home ->
            ( { model
                | page = StartPage
              }
            , Cmd.none
            )

        Just Route.ListFeatures ->
            let
                ( featuresModel, featuresCmd ) =
                    Features.init
            in
            ( { model
                | page = FeaturesPage featuresModel
              }
            , Cmd.map FeaturesMsg featuresCmd
            )

        Just Route.Expenses ->
            let
                ( expensesModel, expensesCmd ) =
                    Expenses.init
            in
            ( { model
                | page = ExpensesPage expensesModel
              }
            , Cmd.map ExpensesMsg expensesCmd
            )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case ( msg, model.page ) of
        ( FeaturesMsg featuresMsg, FeaturesPage features ) ->
            let
                ( updatedModel, cmd ) =
                    Features.update featuresMsg features
            in
            ( { model
                | page = FeaturesPage updatedModel
              }
            , Cmd.map FeaturesMsg cmd
            )

        ( ExpensesMsg expensesMsg, ExpensesPage expenses ) ->
            let
                ( updatedModel, cmd ) =
                    Expenses.update expensesMsg expenses
            in
            ( { model
                | page = ExpensesPage updatedModel
              }
            , Cmd.map ExpensesMsg cmd
            )

        ( ChangedUrl url, _ ) ->
            changeRoute (Route.fromUrl url) model

        ( ClickedLink urlRequest, _ ) ->
            case urlRequest of
                Browser.Internal url ->
                    ( model
                    , Nav.pushUrl model.navKey (Url.toString url)
                    )

                Browser.External href ->
                    ( model
                    , Nav.load href
                    )

        ( _, _ ) ->
            ( model, Cmd.none )


view : Model -> Browser.Document Msg
view model =
    case model.page of
        StartPage ->
            { title = "Start"
            , body =
                [ Header.header model.page
                , Html.div []
                    [ Html.text "Start Page"
                    , Html.a [ Route.href Route.ListFeatures ] [ Html.text "Features" ]
                    ]
                ]
            }

        FeaturesPage features ->
            { title = "Features"
            , body =
                [ Header.header model.page
                , Body.body (Html.map FeaturesMsg (Features.view features))
                ]
            }

        ExpensesPage expenses ->
            { title = "Expenses"
            , body =
                [ Header.header model.page
                , Body.body (Html.map ExpensesMsg (Expenses.view expenses))
                ]
            }

        NotFoundPage ->
            { title = "Page Not Found"
            , body =
                [ Html.div []
                    [ Html.text "page not found"
                    ]
                ]
            }


init : () -> Url -> Nav.Key -> ( Model, Cmd Msg )
init () url key =
    changeRoute (Route.fromUrl url) (initialModel key)


main : Program () Model Msg
main =
    Browser.application
        { init = init
        , update = update
        , view = view
        , subscriptions = \_ -> Sub.none
        , onUrlRequest = ClickedLink
        , onUrlChange = ChangedUrl
        }
