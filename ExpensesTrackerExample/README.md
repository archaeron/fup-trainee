# Expenses Tracker Example

## Installing

We need to install the dependencies of our server.

```
cd expenses-server
npm install
```

## Running

First, start the server in one terminal:

```sh
cd expenses-server
npm run start
```

This will start a very rudimentary server for our expense tracking.

Then, in a second terminal start:

```sh
bin/start-example
```
