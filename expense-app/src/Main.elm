module Main exposing (main)

import Browser
import Message
import Model
import Requests
import Update
import View


init : () -> ( Model.Model, Cmd Message.Message )
init () =
    ( Model.initialModel, Requests.getExpenses |> Cmd.map Message.LoadExpenses )


main : Program () Model.Model Message.Message
main =
    Browser.element
        { init = init
        , update = Update.update
        , view = View.view
        , subscriptions = \_ -> Sub.none
        }
