module AddExpense exposing
    ( Message(..)
    , Model
    , initialModel
    , update
    , view
    )

{-| An encapsulated module describing a counter widget.
-}

import Expense
import Html exposing (Html, button, div, input, label, text)
import Html.Attributes exposing (disabled, style, value)
import Html.Events exposing (onClick, onInput)



--------------------------------------------------------------------------------
-- Model
--------------------------------------------------------------------------------


{-| The `Model` contains all the data necessary
for our widget.
-}
type alias Model =
    { description : String
    , amount : Result String Float
    }


{-| We need an empty `Model` to start with.
-}
initialModel : Model
initialModel =
    { description = ""
    , amount = Err ""
    }



--------------------------------------------------------------------------------
-- Update
--------------------------------------------------------------------------------


type InternalMessage
    = ChangeDescription String
    | ChangeAmount String


{-| Whenever we interact with the widget a message is sent to the
system.
-}
type Message
    = InternalMessage InternalMessage
      -- | Should be handled from outside
    | Save Expense.Expense


{-| We then update the `Model` according to an incoming Message.
-}
update : Message -> Model -> Model
update msg model =
    case msg of
        InternalMessage (ChangeDescription newDescription) ->
            { model | description = newDescription }

        InternalMessage (ChangeAmount newAmount) ->
            let
                amount =
                    case String.toFloat newAmount of
                        Just f ->
                            Ok f

                        Nothing ->
                            Err newAmount
            in
            { model | amount = amount }

        Save _ ->
            model



--------------------------------------------------------------------------------
-- View
--------------------------------------------------------------------------------


{-| The view renders the current state of the widget from
the `Model`.
-}
view : Model -> Html Message
view model =
    div
        [ style "display" "flex"
        , style "flex-direction" "row"
        , style "gap" "0.5rem"
        ]
        [ div
            [ style "display" "flex"
            , style "flex-direction" "column"
            ]
            [ label []
                [ text "Beschreibung" ]
            , input
                [ onInput (ChangeDescription >> InternalMessage), value model.description ]
                []
            ]
        , div
            [ style "display" "flex"
            , style "flex-direction" "column"
            ]
            [ label []
                [ text "Betrag" ]
            , input [ onInput (ChangeAmount >> InternalMessage), value (viewAmount model.amount) ] []
            ]
        , case model.amount of
            Err _ ->
                button
                    [ disabled True ]
                    [ text "+" ]

            Ok f ->
                button
                    [ onClick (Save { description = model.description, amount = f }) ]
                    [ text "+" ]
        ]


viewAmount : Result String Float -> String
viewAmount amountOrErr =
    case amountOrErr of
        Err e ->
            e

        Ok amount ->
            String.fromFloat amount
