module View exposing (..)

import AddExpense
import Expense
import Html exposing (Html, div, li, text, ul)
import Html.Attributes exposing (style)
import Http
import Message
import Model


{-| The view renders the current state of the application from
a `Model`.
-}
view : Model.Model -> Html Message.Message
view model =
    div
        [ style "margin" "2em"
        ]
        [ model.addExpense |> AddExpense.view |> Html.map Message.AddExpenseMessage
        , viewExpenses model.expenses
        , viewHttpError model.httpError
        ]


viewExpenses : Expense.Expenses -> Html message
viewExpenses expenses =
    ul [] (expenses |> List.map viewExpense)


viewExpense : Expense.Expense -> Html message
viewExpense { description, amount } =
    li []
        [ text description
        , text ": "
        , text (String.fromFloat amount)
        ]


viewHttpError : Maybe Http.Error -> Html message
viewHttpError mbError =
    case mbError of
        Nothing ->
            div [] []

        Just error ->
            case error of
                Http.BadUrl url ->
                    div [] [ text ("Invalid url: " ++ url) ]

                Http.Timeout ->
                    div [] [ text "The network timed out" ]

                Http.NetworkError ->
                    div [] [ text "There was a network error" ]

                Http.BadStatus status ->
                    div [] [ text ("Didn't get a successfull status back: " ++ String.fromInt status) ]

                Http.BadBody bodyParseMessage ->
                    div [] [ text ("The response could not be parsed: " ++ bodyParseMessage) ]
