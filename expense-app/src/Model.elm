module Model exposing (..)

import AddExpense
import Expense
import Http


type alias Model =
    { addExpense : AddExpense.Model
    , expenses : Expense.Expenses
    , httpError : Maybe Http.Error
    }


initialModel : Model
initialModel =
    { addExpense = AddExpense.initialModel
    , expenses = []
    , httpError = Nothing
    }
