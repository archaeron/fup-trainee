module Update exposing (..)

import AddExpense
import Message
import Model
import Requests


update : Message.Message -> Model.Model -> ( Model.Model, Cmd Message.Message )
update message model =
    case message of
        Message.AddExpenseMessage addExpenseMessage ->
            case addExpenseMessage of
                AddExpense.InternalMessage _ ->
                    ( { model
                        | addExpense = AddExpense.update addExpenseMessage model.addExpense
                      }
                    , Cmd.none
                    )

                AddExpense.Save newExpense ->
                    ( model
                    , Requests.postExpense newExpense |> Cmd.map Message.ExpenseAdded
                    )

        Message.LoadExpenses loadedExpenses ->
            case loadedExpenses of
                Err err ->
                    ( { model | httpError = Just err }, Cmd.none )

                Ok expenses ->
                    ( { model
                        | expenses = expenses
                        , httpError = Nothing
                      }
                    , Cmd.none
                    )

        Message.ExpenseAdded expenseAdded ->
            case expenseAdded of
                Err err ->
                    ( { model | httpError = Just err }, Cmd.none )

                Ok expense ->
                    ( { model
                        | addExpense = AddExpense.initialModel
                        , expenses = expense :: model.expenses
                        , httpError = Nothing
                      }
                    , Cmd.none
                    )
