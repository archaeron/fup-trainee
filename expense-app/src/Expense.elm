module Expense exposing (..)

import Json.Decode as Decode
import Json.Encode as Encode


type alias Expense =
    { description : String
    , amount : Float
    }


type alias Expenses =
    List Expense



------------------------------------------------------------------------------------------------------------------------
-- JSON
------------------------------------------------------------------------------------------------------------------------


expenseDecoder : Decode.Decoder Expense
expenseDecoder =
    Decode.map2
        (\amount description ->
            { amount = amount
            , description = description
            }
        )
        (Decode.field "amount" Decode.float)
        (Decode.field "description" Decode.string)


expensesDecoder : Decode.Decoder Expenses
expensesDecoder =
    Decode.list expenseDecoder


expenseEncoder : Expense -> Encode.Value
expenseEncoder { amount, description } =
    Encode.object
        [ ( "description", Encode.string description )
        , ( "amount", Encode.float amount )
        ]
