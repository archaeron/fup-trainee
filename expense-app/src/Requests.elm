module Requests exposing (..)

import Expense exposing (Expense, Expenses, expenseDecoder, expenseEncoder, expensesDecoder)
import Http


getExpenses : Cmd (Result Http.Error Expenses)
getExpenses =
    Http.get
        { url = "http://localhost:3000/expenses"
        , expect =
            Http.expectJson identity expensesDecoder
        }


postExpense : Expense -> Cmd (Result Http.Error Expense)
postExpense expense =
    Http.post
        { url = "http://localhost:3000/expenses"
        , body =
            Http.jsonBody (expenseEncoder expense)
        , expect =
            Http.expectJson identity expenseDecoder
        }
