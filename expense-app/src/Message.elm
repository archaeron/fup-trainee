module Message exposing (..)

import AddExpense
import Expense
import Http


type Message
    = AddExpenseMessage AddExpense.Message
    | LoadExpenses (Result Http.Error Expense.Expenses)
    | ExpenseAdded (Result Http.Error Expense.Expense)
