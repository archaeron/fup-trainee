# Example Todo App

An example of a todo app that saves it's state on a server.

## Development

From this folder either do

```sh
elm reactor
```

or use

```sh
elm-live src/Main.elm --open -- --debug
```

for the time travelling debugger.
