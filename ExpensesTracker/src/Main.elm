module Main exposing (..)

import Browser
import Browser.Navigation as Nav
import Components as C
import Html
import Url exposing (Url)



--------------------------------------------------------------------------------
-- Model
--------------------------------------------------------------------------------


type alias Model =
    ()



--------------------------------------------------------------------------------
-- Update
--------------------------------------------------------------------------------


type alias Msg =
    ()


init : () -> Url -> Nav.Key -> ( Model, Cmd Msg )
init () _ _ =
    ( (), Cmd.none )


update : Msg -> Model -> ( Model, Cmd Msg )
update () () =
    ( (), Cmd.none )



--------------------------------------------------------------------------------
-- View
--------------------------------------------------------------------------------


view : Model -> Browser.Document Msg
view () =
    { title = "Expense Tracker"
    , body =
        [ Html.div
            []
            [ Html.h1 []
                [ Html.text "Expense Tracker"
                ]
            , C.spinner
            ]
        ]
    }



--------------------------------------------------------------------------------
-- Main
--------------------------------------------------------------------------------


main : Program () Model Msg
main =
    Browser.application
        { init = init
        , update = update
        , view = view
        , subscriptions = \_ -> Sub.none
        , onUrlRequest = \_ -> ()
        , onUrlChange = \_ -> ()
        }
