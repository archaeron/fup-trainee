# Elm Architecture V2

Reusing components and using values
from the component in the main module.

## Development

From this folder either do

```sh
elm reactor
```

or use

```sh
elm-live src/Main.elm --open -- --debug
```

for the time travelling debugger.

## Exercise

Building on the exercise from 'V2', add a box where the sum of all
successful input fields is displayed.

Hints:
- https://package.elm-lang.org/packages/elm/core/latest/Result#withDefault
- https://package.elm-lang.org/packages/elm/core/latest/List#sum
