module Counter exposing
    ( CounterModel
    , CounterMsg
    , initialModel
    , update
    , view
    )

{-| An encapsulated module describing a counter widget.
-}

import Html exposing (Html, button, div, text)
import Html.Events exposing (onClick)



--------------------------------------------------------------------------------
-- Model
--------------------------------------------------------------------------------


{-| The `Model` contains all the data necessary
for our widget.
-}
type alias CounterModel =
    { number : Int }


{-| We need an empty `Model` to start with.
-}
initialModel : Int -> CounterModel
initialModel n =
    { number = n }



--------------------------------------------------------------------------------
-- Update
--------------------------------------------------------------------------------


{-| Whenever we interact with the widget a message is sent to the
system.
-}
type CounterMsg
    = Increment
    | Decrement


{-| We then update the `Model` according to an incoming Message.
-}
update : CounterMsg -> CounterModel -> CounterModel
update msg model =
    case msg of
        Increment ->
            let
                updatedNumber =
                    model.number + 1
            in
            { model | number = updatedNumber }

        Decrement ->
            let
                updatedNumber =
                    model.number - 1
            in
            { model | number = updatedNumber }



--------------------------------------------------------------------------------
-- View
--------------------------------------------------------------------------------


{-| The view renders the current state of the widget from
the `Model`.
-}
view : CounterModel -> Html CounterMsg
view model =
    div []
        [ button [ onClick Increment ] [ text "+" ]
        , div [] [ text (String.fromInt (.number model)) ]
        , button [ onClick Decrement ] [ text "-" ]
        ]
