module Main exposing (..)

import Browser
import Counter
import Html exposing (Html, div, text)



--------------------------------------------------------------------------------
-- Model
--------------------------------------------------------------------------------


{-| The `Model` contains all the data necessary
to display our application.
Here we use the `Counter.Model` types from the two nested widgets.
-}
type alias Model =
    { counter1Model : Counter.CounterModel
    , counter2Model : Counter.CounterModel
    }


{-| We need an empty `Model` to start with.
-}
initialModel :
    { counter1Init : Int, counter2Init : Int }
    -> Model
initialModel { counter1Init, counter2Init } =
    { counter1Model = Counter.initialModel counter1Init
    , counter2Model = Counter.initialModel counter2Init
    }



--------------------------------------------------------------------------------
-- Update
--------------------------------------------------------------------------------


{-| Each widget will send their own `Counter.Msg` and we need to keep
track where it came from.
-}
type Msg
    = Counter1Msg Counter.CounterMsg
    | Counter2Msg Counter.CounterMsg


{-| We then update the `Model` according to an incoming Message.
What we do is we use the `Counter.update` function to update the
model in the nested widgets.
-}
update : Msg -> Model -> Model
update msg model =
    case msg of
        Counter1Msg counterMsg ->
            let
                counter1Model =
                    Counter.update counterMsg model.counter1Model
            in
            { model
                | counter1Model = counter1Model
            }

        Counter2Msg counterMsg ->
            let
                counter2Model =
                    Counter.update counterMsg model.counter2Model
            in
            { model
                | counter2Model = counter2Model
            }



--------------------------------------------------------------------------------
-- View
--------------------------------------------------------------------------------


{-| The view renders the current state of the application from
a `Model`, using the `Counter.view` function.
This is the most complicated function of this example.
What is important to understand here is that `Counter.view` will return
a value of type `Counter.Msg`, but what we need here is something of type
`Msg`.
-}
view : Model -> Html Msg
view model =
    div []
        [ model |> .counter1Model |> Counter.view |> Html.map Counter1Msg
        , model |> .counter2Model |> Counter.view |> Html.map Counter2Msg
        , div []
            [ text "Sum:", text (String.fromInt (model.counter1Model.number + model.counter2Model.number)) ]
        ]



--------------------------------------------------------------------------------
-- Main
--------------------------------------------------------------------------------


{-| We put all pieces together in the `main` function.
-}
main : Program () Model Msg
main =
    Browser.sandbox
        { init = initialModel { counter1Init = 2, counter2Init = 5 }
        , update = update
        , view = view
        }
