# Elm Architecture V3

An example of a dynamic web page that makes requests to a server.

## Development

Run

Open a terminal. Change to the `servers/reverse-server` directory.
If this is the first time you run this server, execute: `npm install`.
Then execute: `npm run start` to run the example server.

In a new termina, go to `ElmArchitecture/V3`. From this folder either do

```sh
elm reactor
```

or use

```sh
elm-live src/Main.elm --open -- --debug
```

for the time travelling debugger.

## Exercise

See `src/ExpensesTracker/README.md`.
