module View exposing (..)

{-|


## View

-}

import Html exposing (Html, div, input, text)
import Html.Attributes exposing (value)
import Html.Events exposing (onInput)
import Model
import Msg


viewResult : Model.WordAndReverse -> Html Msg.Msg
viewResult { word, reversedWord } =
    div []
        [ div []
            [ text "Word:", text word ]
        , div
            []
            [ text "Reversed:", text reversedWord ]
        ]


{-| The view renders the current state of the application from
a `Model`.
-}
view : Model.Model -> Html Msg.Msg
view { inputString, requestResult } =
    div []
        [ input [ onInput Msg.UpdateInputString, value inputString ] []
        , viewResult requestResult
        ]
