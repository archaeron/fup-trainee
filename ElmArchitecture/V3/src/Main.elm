module Main exposing (..)

{-|


## Main

-}

import Browser
import Model
import Msg
import Update
import View


{-| We put all pieces together in the `main` function.
-}
main : Program () Model.Model Msg.Msg
main =
    Browser.element
        { init = \() -> ( Model.initialModel, Cmd.none )
        , update = Update.update
        , view = View.view
        , subscriptions = always Sub.none
        }
