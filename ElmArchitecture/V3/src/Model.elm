module Model exposing
    ( Model
    , WordAndReverse
    , initialModel
    , wordAndReverseDecoder
    )

{-|


## Model

-}

import Json.Decode exposing (Decoder, field, map2, string)



--------------------------------------------------------------------------------
-- Model
--------------------------------------------------------------------------------


type alias WordAndReverse =
    { word : String
    , reversedWord : String
    }


{-| The `Model` contains all the data necessary
to display our application.
-}
type alias Model =
    { inputString : String
    , requestResult : WordAndReverse
    }


{-| We need an empty `Model` to start with.
-}
initialModel : Model
initialModel =
    { inputString = ""
    , requestResult =
        { word = ""
        , reversedWord = ""
        }
    }



--------------------------------------------------------------------------------
-- Decoders
--------------------------------------------------------------------------------


wordAndReverseDecoder : Decoder WordAndReverse
wordAndReverseDecoder =
    map2
        (\word reversedWord ->
            { word = word, reversedWord = reversedWord }
        )
        (field "word" string)
        (field "reversedWord" string)
