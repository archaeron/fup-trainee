module Msg exposing (Msg(..))

{-| Whenever we interact with the page a message is sent to the
system.
-}

import Model exposing (WordAndReverse)
import Http


type Msg
    = UpdateInputString String
    | ReceiveSuccess WordAndReverse
    | ReceiveError Http.Error
