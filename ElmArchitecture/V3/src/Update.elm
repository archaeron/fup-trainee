module Update exposing (update)

{-|


## Update

-}

import Model as Model
import Msg as Msg
import Requests as Requests


{-| We then update the `Model` according to an incoming Message.
-}
update : Msg.Msg -> Model.Model -> ( Model.Model, Cmd Msg.Msg )
update msg model =
    case msg of
        Msg.ReceiveSuccess wordAndReverse ->
            ( { model | requestResult = wordAndReverse }
            , Cmd.none
            )

        Msg.ReceiveError _ ->
            ( model
            , Cmd.none
            )

        Msg.UpdateInputString str ->
            ( { model | inputString = str }
            , Requests.getReversedWords str
            )
