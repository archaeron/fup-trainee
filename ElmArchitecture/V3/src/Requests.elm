module Requests exposing (..)

import Model as Model
import Msg as Msg
import Http


resultToMsg : Result Http.Error Model.WordAndReverse -> Msg.Msg
resultToMsg result =
    case result of
        Ok success ->
            Msg.ReceiveSuccess success

        Err error ->
            Msg.ReceiveError error


getReversedWords : String -> Cmd Msg.Msg
getReversedWords word =
    Http.get
        { url = "http://localhost:3000/reverse/" ++ word
        , expect = Http.expectJson resultToMsg Model.wordAndReverseDecoder
        }
