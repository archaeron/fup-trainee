# Elm Architecture V1

An example of a dynamic web page.

## Development

From this folder either do

```sh
elm reactor
```

or use

```sh
elm-live src/Main.elm --open -- --debug
```

for the time travelling debugger.

## Exercise

Make the input field parse its content into an `Int`.
Display a box with an error message if the `Int` could not be parsed.

Important: Even if the input is not an `Int` we don't want to lose it.
Make sure the value stays in the input field even if it can not be parsed.

Hints:
- https://package.elm-lang.org/packages/elm/html/latest/Html-Events#onInput
- https://package.elm-lang.org/packages/elm/core/latest/String#toFloat
- https://package.elm-lang.org/packages/elm/core/latest/Result#Result
