module Main exposing (main)

import Browser
import Html exposing (Html, button, div, text)
import Html.Events exposing (onClick)



--------------------------------------------------------------------------------
-- Model
--------------------------------------------------------------------------------


{-| The `Model` contains all the data necessary
to display our application.
-}
type alias Model =
    { number : Int }


{-| We need an empty `Model` to start with.
-}
initialModel : Model
initialModel =
    { number = 0 }



--------------------------------------------------------------------------------
-- Update
--------------------------------------------------------------------------------


{-| Whenever we interact with the page a message is sent to the
system.
-}
type Msg
    = Increment
    | Decrement


{-| We then update the `Model` according to an incoming Message.
-}
update : Msg -> Model -> Model
update msg model =
    case msg of
        Increment ->
            { model | number = .number model + 1 }

        Decrement ->
            { model | number = .number model - 1 }



--------------------------------------------------------------------------------
-- View
--------------------------------------------------------------------------------


{-| The view renders the current state of the application from
a `Model`.
-}
view : Model -> Html Msg
view model =
    div []
        [ button [ onClick Increment ] [ text "+" ]
        , div [] [ text (String.fromInt (.number model)) ]
        , button [ onClick Decrement ] [ text "-" ]
        ]



--------------------------------------------------------------------------------
-- Main
--------------------------------------------------------------------------------


{-| We put all pieces together in the `main` function.
-}
main : Program () Model Msg
main =
    Browser.sandbox
        { init = initialModel
        , update = update
        , view = view
        }
