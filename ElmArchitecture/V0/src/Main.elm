module Main exposing (main)

{-| Non-interactive Elm page.
-}

import Html exposing (Html, button, div, text)


{-| Rendering the view.
-}
view : Html msg
view =
    div []
        [ button [] [ text "+" ]
        , div [] [ text (String.fromInt 0) ]
        , button [] [ text "-" ]
        ]


main : Html msg
main =
    view
