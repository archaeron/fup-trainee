# Elm Architecture V0

An example of a static web page.

## Development

From this folder either do

```sh
elm reactor
```

or use

```sh
elm-live src/Main.elm --open --host 0.0.0.0 -- --debug
```

for the time travelling debugger.

## Exercise

Open the `Main.elm` file in the `Exercise` folder and
create a simple page with a header and an input field.

Hints:
- https://package.elm-lang.org/packages/elm/html/latest/Html#h1
- https://package.elm-lang.org/packages/elm/html/latest/Html#input
