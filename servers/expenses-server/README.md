# Expenses Server

The expenses server is a very small server
for the ExpensesTracker exercise.
All data is saved in memory, which means
it will keep your input data as long as it's
running, but you can just reset it by
stopping and starting it again.

## API

### Features

Features are named groups of tags.
E.g.:

- users
  - Alice
  - Bob
- expense-type
  - food
  - gadgets
  - washing
  - ...

Endpoints:

- POST /feature
  - request: { name : String, options : List String }
  - response: { name : String, options : List String }
- GET /feature
  - reponse:
    { [name:string]:
        { name : String, options : List String }
    }

### Expenses

An expense is something that had to be paid.
It consists of a description, some tags and an amount.

```elm
type alias FeatureSelection =
    { feature : String
    , option : String
    }


type alias Expense =
    { amount : Float
    , description : String
    , features : List FeatureSelection
    }
```

- POST /expense
    - request: Expense
    - response: Expense
- GET /expense
    - response: List Expense
