var express = require('express')
var cors = require('cors')
var app = express()

const port = 3000;

var database = {
    expenses: []
}

var corsOptions = {
    origin: '*',
}

app.use(cors(corsOptions))
app.use(express.json())

app.get('/expenses', function (_req, res) {
    console.log("Getting all expenses", database.expenses)
    res.json(database.expenses)
})

app.post('/expenses', function (req, res) {
    const newExpense = req.body
    console.log("Creating a new expense", newExpense)
    database.expenses.push(newExpense)
    res.json(newExpense)
})

app.listen(port, () => {
    console.log(`Example app listening on port ${port}!`)
});
