var express = require('express')
var cors = require('cors')
var app = express()
const port = 3000;

var corsOptions = {
    origin: '*',
}

app.use(cors(corsOptions))

app.get('/reverse/:word', function (req, res) {
    const word = req.params.word
    const reversedWord = word.split("").reverse().join("")
    res.send({ word, reversedWord })
})

app.listen(port, () => {
    console.log(`Example app listening on port ${port}!`)
});
