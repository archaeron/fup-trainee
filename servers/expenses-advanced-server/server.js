var express = require('express')
var cors = require('cors')
var app = express()

const port = 3000;

var database = {
    features: {},
    expenses: []
}

var corsOptions = {
    origin: '*',
}

app.use(cors(corsOptions))
app.use(express.json())

app.get('/feature', function (_req, res) {
    console.log("Getting all Features", database.features)
    res.json(database.features)
})

app.post('/feature', function (req, res) {
    const newFeature = req.body
    const name = newFeature.name
    if (name) {
        console.log("Creating a new feature", newFeature)
        database.features[name] = newFeature
        res.json(newFeature)
    } else {
        console.error("Could not create a new feature")
        res.sendStatus(400)
    }
})

app.get('/feature/:name', function (req, res) {
    const name = req.params.name
    const feature = database.features[name]
    if (feature) {
        console.log("Getting feature", feature)
        res.json(feature)
    }
    else {
        console.error("Could not get feature with name", name)
        res.sendStatus(404)
    }
})

app.get('/expense', function (_req, res) {
    console.log("Getting all expenses", database.expenses)
    res.json(database.expenses)
})

app.post('/expense', function (req, res) {
    const newExpense = req.body
    console.log("Creating a new expense", newExpense)
    database.expenses.push(newExpense)
    res.json(newExpense)
})

app.listen(port, () => {
    console.log(`Example app listening on port ${port}!`)
});
